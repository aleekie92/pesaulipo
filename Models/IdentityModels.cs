﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace pesaulipo.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        public string Factory
        {
            get;
            set;
        }

        public string Region
        {
            get;
            set;
        }

        public string AgentCode
        {
            get;
            set;
        }
        public string UserType
        {
            get;
            set;
        }
        public string Otp
        {
            get;
            set;
        }
        public bool Status
        {
            get;
            set;
        }
        public bool Enabled
        {
            get;
            set;
        }
        public bool OtpConfirmed
        {
            get;
            set;
        }

        public string LastSuccessLoginTime
        {
            get;
            set;
        }
     
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("PesaUlipoConnectionString", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}