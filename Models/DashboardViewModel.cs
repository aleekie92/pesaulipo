﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pesaulipo.Models
{
    public class DashboardViewModel
    {

		public int AgencyCount
		{
			get;
			set;
		}

		public int AgentCount
		{
			get;
			set;
		}

		public int DeviceCount
		{
			get;
			set;
		}


		public int LoanApplicationCount
		{
			get;
			set;
		}

		

		public int OthersCount
		{
			get;
			set;
		}

		

		public int SaccoTransactionDailyCount
		{
			get;
			set;
		}

		public int SaccoTransactionWeeklyCount
		{
			get;
			set;
		}

		

		public int TransactionCount
		{
			get;
			set;
		}

		public int TransactionDaily
		{
			get;
			set;
		}

		public int Transactions100MinsAgo
		{
			get;
			set;
		}

		public int Transactions120MinsAgo
		{
			get;
			set;
		}

		public int Transactions140MinsAgo
		{
			get;
			set;
		}

		public int Transactions160MinsAgo
		{
			get;
			set;
		}

		public int Transactions180MinsAgo
		{
			get;
			set;
		}

		public int Transactions200MinsAgo
		{
			get;
			set;
		}

		public int Transactions20MinsAgo
		{
			get;
			set;
		}

		public int Transactions40MinsAgo
		{
			get;
			set;
		}

		public int Transactions60MinsAgo
		{
			get;
			set;
		}

		public int Transactions80MinsAgo
		{
			get;
			set;
		}

		public int TransactionWeeklyCount
		{
			get;
			set;
		}

	
		
		public int MemberRegCount
		{
			get;
			set;
		}
		public int SaccoTransactionCount
		{
			get;
			set;
		}

		public int openLoanApplication
		{
			get;
			set;
		}
		public int pendingLoanApplication
		{
			get;
			set;
		}
		public int approvedLoanApplication
		{
			get;
			set;
		}
		public int rejectedLoanApplication
		{
			get;
			set;
		}
	}
}