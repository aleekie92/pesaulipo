﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace pesaulipo.Models
{
    public class WebServiceTransactionModel
    {

		public List<SelectListItem> DropDownForm = new List<SelectListItem>();

		public List<SelectListItem> transactionType = new List<SelectListItem>();

		public List<SelectListItem> AgentDropDownForm = new List<SelectListItem>();

		public List<SelectListItem> RegionDropDownForm = new List<SelectListItem>();
		public List<SelectListItem> StatusDropDown = new List<SelectListItem>();

		public List<SelectListItem> FactoryDropDownForm = new List<SelectListItem>();
		public string timestamp { get; set; }
		public string Document_no { get; set; }
		public string transaction_date { get; set; }
		public string account_no { get; set; }
		public string Description { get; set; }
		public string Amount { get; set; }
		public string Transaction_type { get; set; }
		public string Transaction_time { get; set; }
		public string date_posted { get; set; }
		public string time_posted { get; set; }
		public string account_status { get; set; }
		public string changed { get; set; }
		public string date_changed { get; set; }
		public string time_changed { get; set; }
		public string changed_by { get; set; }
		public string approved_by { get; set; }
		public string branch_code { get; set; }
		public string activity_code { get; set; }
		public string global_dimension_filter1 { get; set; }
		public string global_dimension_filter2 { get; set; }//to be added
		public string agent_code { get; set; }
		public string agent_name { get; set; }
		public string loan_no { get; set; }
		public string acc_name { get; set; }
		public string telephone { get; set; }
		public string id_no { get; set; }
		public string grower_no { get; set; }
		public string approval_status { get; set; }
		public string device_id { get; set; }
		public string gender { get; set; }
		public string dob { get; set; }
		public string loan_product { get; set; }
		public string repayment_period { get; set; }
		public string maximum_qualified { get; set; }
		public string qualified_kg { get; set; }
		public string unfinished_loan { get; set; }
		public string loan_product_name { get; set; }
		public string amount_to_guarantee { get; set; }
		public string mode_of_disbursment { get; set; }
		public string bank_code { get; set; }
		public string bank_name { get; set; }
		public string bank_acc_no { get; set; }
		public string bank_branch { get; set; }
		public string laon_installments { get; set; }
		public string citizenship { get; set; }
		public string register_pu { get; set; }
		public string next_of_kin { get; set; }
		public string relationship_next_of_kin { get; set; }
		public string next_of_kin_id { get; set; }
		public string guarantor_grower_no { get; set; }
		public string applicant_net_amount { get; set; }
		public string approved_time { get; set; }
		public string approved_date { get; set; }
		public string pu_registration_language { get; set; }
		public string sacco_code { get; set; }
		public string sacco_acc_no { get; set; }

		[Display(Name = "Transaction")]
		public string TransactionTypeFilter
		{
			get;
			set;
		}

		[Display(Name = "Region")]
		public string RegionFilter
		{
			get;
			set;
		}

		[Display(Name = "Status")]
		public string StatusFilter
		{
			get;
			set;
		}

		[Display(Name = "Factory")]
		public string FactoryFilter
		{
			get;
			set;
		}

		[Display(Name = "Date From")]
		public string DateFilterFrom
		{
			get;
			set;
		}

		[Display(Name = "Date To")]
		public string DateFilterTo
		{
			get;
			set;
		}

		[Display(Name = "  Agent ")]
		public string AgentFilter
		{
			get;
			set;
		}

		public List<WebServiceTransactionModel> transactionsList { get; set; }
		public List<GFL_Live_Agent_Application> AgentListNew { get; set; }
		public WebServiceTransactionModel()
		{
		}

	}
}