﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pesaulipo.Models
{
	public class ReportsSaccoModel
	{
		public int SaccoAccountActivationCount
		{
			get;
			set;
		}

		public int SaccoBalanceEnquiryCount
		{
			get;
			set;
		}

		public int SaccoDepositCount
		{
			get;
			set;
		}

		public int SaccoLoanRepaymentCount
		{
			get;
			set;
		}

		public int SaccoMemberRegistrationCount
		{
			get;
			set;
		}

		public int SaccoMinistatementCount
		{
			get;
			set;
		}

		public string SaccoName
		{
			get;
			set;
		}

		public int SaccoTransactionsCount
		{
			get;
			set;
		}

		public int SaccoWithdrawalCount
		{
			get;
			set;
		}

		public ReportsSaccoModel()
		{
		}
	}
}