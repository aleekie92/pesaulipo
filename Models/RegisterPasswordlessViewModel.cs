﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System;
using System.ComponentModel.DataAnnotations;

namespace pesaulipo.Models
{
    public class RegisterPasswordlessViewModel
    {

		[Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
		[DataType(DataType.Password)]
		[Display(Name = "Confirm password")]
		public string ConfirmPassword
		{
			get;
			set;
		}

		[Display(Name = "Enter Your Email :")]
		[EmailAddress]
		[Required]
		public string Email
		{
			get;
			set;
		}



		[Display(Name = "Agent Code")]
		public string AgentCode
		{
			get;
			set;
		}

		[Display(Name = "Phone Number")]
		public string PhoneNo
		{
			get;
			set;
		}


		[Display(Name = "Region")]
		public string Region
		{
			get;
			set;
		}

		[Display(Name = "Factory")]
		public string Factory
		{
			get;
			set;
		}

		[Display(Name = "AccessFailedCount")]
		public int AccessFailedCount
		{
			get;
			set;
		}

		[Display(Name = "LastSuccessLoginTime")]
		public string LastSuccessLoginTime
		{
			get;
			set;
		}
		public string Name
		{
			get;
			set;
		}

		[Display(Name = "User Role")]
		public string Role
		{
			get;
			set;
		}

		[DataType(DataType.Password)]
		[Display(Name = "Password")]
		[StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
		public string Password
		{
			get;
			set;
		}
		public RegisterPasswordlessViewModel()
		{
		}

	}
}