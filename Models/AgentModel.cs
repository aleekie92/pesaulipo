﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pesaulipo.Models
{
	public class AgentModel
	{
		public bool? Active
		{
			get;
			set;
		}

		public string name { get; set; }

		public string AgentCode
		{
			get;
			set;
		}

		public string Email
		{
			get;
			set;
		}

		public string UserType
		{
			get;
			set;
		}


		public string Region
		{
			get;
			set;
		}

		public string Factory
		{
			get;
			set;
		}
		

		public string BankNo
		{
			get;
			set;
		}

		public DateTime? DateRegistered
		{
			get;
			set;
		}

		public string DeviceID
		{
			get;
			set;
		}

		public int EntryNo
		{
			get;
			set;
		}

		public bool? Internal
		{
			get;
			set;
		}

		public string Location
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		public int status
		{
			get;
			set;
		}

		public string deviceID
		{
			get;
			set;
		}

		public string Password
		{
			get;
			set;
		}

		public bool? PasswordChanged
		{
			get;
			set;
		}

		public string RegisteredBy
		{
			get;
			set;
		}

		public string Telephone
		{
			get;
			set;
		}

		public DateTime? TimeRegistred
		{
			get;
			set;
		}

		public AgentModel()
		{
		}
	}
}