﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace pesaulipo.Models
{
	public class Reports
	{
		public List<SelectListItem> DropDownForm = new List<SelectListItem>();

		public List<SelectListItem> SaccoDropDownForm = new List<SelectListItem>();

		public List<SelectListItem> AgentDropDownForm = new List<SelectListItem>();

		public List<AgencyTransactionsModels> AgencyTransactions
		{
			get;
			set;
		}

		public string AgentFilter
		{
			get;
			set;
		}

		public List<ReportsAgentModel> AgentModelCounts
		{
			get;
			set;
		}

		public List<GFL_Live_Agent_Application> AgentNameList
		{
			get;
			set;
		}

		public int AllLoanApplicationCount1001000
		{
			get;
			set;
		}

		public int AllLoanApplicationCount100125000
		{
			get;
			set;
		}

		public int AllSaccosGurantorNominationCoretecShare
		{
			get;
			set;
		}

		public int AllSaccosGurantorNominationCount
		{
			get;
			set;
		}

		public int AllSaccosBalanceEnquiryCoretecShare
		{
			get;
			set;
		}

		public int AllSaccosBalanceEnquiryCount
		{
			get;
			set;
		}

		public int AllSaccosLoanApplicationCoretecShare
		{
			get;
			set;
		}

		public int AllSaccosLoanApplicationCount
		{
			get;
			set;
		}

		public int AllSaccosLoanRepaymentCoretecShare
		{
			get;
			set;
		}

		public int AllSaccosLoanRepaymentCount
		{
			get;
			set;
		}

		public int AllSaccosMemberRegistrationCoretecShare
		{
			get;
			set;
		}

		public int AllSaccosMemberRegistrationCount
		{
			get;
			set;
		}

		public int AllSaccosMinistatementCoretecShare
		{
			get;
			set;
		}

		public int AllSaccosMinistatementCount
		{
			get;
			set;
		}

		public int AllSaccosTransactionsCoretecShare
		{
			get;
			set;
		}

		public int AllSaccosTransactionsCount
		{
			get;
			set;
		}

		public int AllSaccosMemberactivationCoretecShare
		{
			get;
			set;
		}

		public int AllSaccosMemberactivationCount
		{
			get;
			set;
		}

		public int AllSaccosMemberactivationCount1001000
		{
			get;
			set;
		}

		public int AllSaccosMemberactivationCount10013000
		{
			get;
			set;
		}

		public int AllSaccosMemberactivationCount1500125000
		{
			get;
			set;
		}

		public int AllSaccosMemberactivationCount30016000
		{
			get;
			set;
		}

		public int AllSaccosMemberactivationCount600115000
		{
			get;
			set;
		}

		[Display(Name = "Date From")]
		public string DateFilterFrom
		{
			get;
			set;
		}

		[Display(Name = "Date To")]
		public string DateFilterTo
		{
			get;
			set;
		}

		public int ParticularSaccoGurantorNominationCoretecShare
		{
			get;
			set;
		}

		public int ParticularSaccoGurantorNominationCount
		{
			get;
			set;
		}

		public int ParticularSaccoBalanceEnquiryCoretecShare
		{
			get;
			set;
		}

		public int ParticularSaccoBalanceEnquiryCount
		{
			get;
			set;
		}

		
		public int ParticularSaccoLoanApplicationCount
		{
			get;
			set;
		}

		public int ParticularSaccoLoanApplicationCount1000015000
		{
			get;
			set;
		}

		public int ParticularSaccoLoanApplicationCount10003000
		{
			get;
			set;
		}

		public int ParticularSaccoLoanApplicationCount1500025000
		{
			get;
			set;
		}

		public int ParticularSaccoLoanApplicationCount2500050000
		{
			get;
			set;
		}

		public int ParticularSaccoLoanApplicationCount30006000
		{
			get;
			set;
		}

		public int ParticularSaccoLoanApplicationCount50000100000
		{
			get;
			set;
		}

		public int ParticularSaccoLoanApplicationCount501000
		{
			get;
			set;
		}

		public int ParticularSaccoLoanApplicationCount600010000
		{
			get;
			set;
		}

		public int ParticularSaccoLoanRepaymentCoretecShare
		{
			get;
			set;
		}

		public int ParticularSaccoLoanRepaymentCount
		{
			get;
			set;
		}

		public int ParticularSaccoMemberRegistrationCoretecShare
		{
			get;
			set;
		}

		public int ParticularSaccoMemberRegistrationCount
		{
			get;
			set;
		}

		public int ParticularSaccoMinistatementCoretecShare
		{
			get;
			set;
		}

		public int ParticularSaccoMinistatementCount
		{
			get;
			set;
		}

		public int ParticularSaccoTransactionsCoretecShare
		{
			get;
			set;
		}

		public int ParticularSaccoTransactionsCount
		{
			get;
			set;
		}

		public int ParticularSaccoMemberactivationCoretecShare
		{
			get;
			set;
		}

		public int ParticularSaccoMemberactivationCount
		{
			get;
			set;
		}

		public int ParticularSaccoMemberactivationCount1000015000
		{
			get;
			set;
		}

		public int ParticularSaccoMemberactivationCount1001000
		{
			get;
			set;
		}

		public int ParticularSaccoMemberactivationCount10013000
		{
			get;
			set;
		}

		public int ParticularSaccoMemberactivationCount1500125000
		{
			get;
			set;
		}

		public int ParticularSaccoMemberactivationCount30016000
		{
			get;
			set;
		}

		public int ParticularSaccoMemberactivationCount600110000
		{
			get;
			set;
		}

		public Reports ReportStorage
		{
			get;
			set;
		}

		public int SaccoAgentGurantorNominationCount
		{
			get;
			set;
		}

		public int SaccoAgentBalanceEnquiryCount
		{
			get;
			set;
		}

		public int SaccoAgentLoanApplicationCount
		{
			get;
			set;
		}

		public int SaccoAgentLoanRepaymentCount
		{
			get;
			set;
		}

		public int SaccoAgentMemberRegistrationCount
		{
			get;
			set;
		}

		public int SaccoAgentMinistatementCount
		{
			get;
			set;
		}

		public int SaccoAgentsGurantorNominationCount
		{
			get;
			set;
		}

		public int openRegistrationCount
		{
			get;
			set;
		}
		public int pendingRegistrationCount
		{
			get;
			set;
		}
		public int approvedRegistrationCount
		{
			get;
			set;
		}
		public int rejectedRegistrationCount
		{
			get;
			set;
		}
		public int openLoanApplication
		{
			get;
			set;
		}
		public int pendingLoanApplication
		{
			get;
			set;
		}
		public int approvedLoanApplication
		{
			get;
			set;
		}
		public int rejectedLoanApplication
		{
			get;
			set;
		}
		public int SaccoAgentsBalanceEnquiryCount
		{
			get;
			set;
		}

		public int SaccoAgentsLoanApplicationCount
		{
			get;
			set;
		}

		public int SaccoAgentsLoanRepaymentCount
		{
			get;
			set;
		}

		public int SaccoAgentsMemberRegistrationCount
		{
			get;
			set;
		}

		public int SaccoAgentsMinistatementCount
		{
			get;
			set;
		}

		public int SaccoAgentsTransactionsCount
		{
			get;
			set;
		}

		public int SaccoAgentsMemberactivationCount
		{
			get;
			set;
		}

		public int SaccoAgentTransactionsCount
		{
			get;
			set;
		}

		
		public int SaccoAgentMemberactivationCount
		{
			get;
			set;
		}

		public string SaccoFilter
		{
			get;
			set;
		}
		

		public List<ReportsSaccoModel> saccoModelCounts
		{
			get;
			set;
		}

	
		public string SaccoNameTitle
		{
			get;
			set;
		}

		public string saccoPar
		{
			get;
			set;
		}

		public string TransactionTypeFilter
		{
			get;
			set;
		}

		public Reports()
		{
		}
	}
}