﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace pesaulipo.Models
{
	public class AgencyTransactionsModels
	{
		public List<SelectListItem> DropDownForm = new List<SelectListItem>();

		public List<SelectListItem> transactionType = new List<SelectListItem>();

		public List<SelectListItem> AgentDropDownForm = new List<SelectListItem>();

		public List<SelectListItem> RegionDropDownForm = new List<SelectListItem>();

		public List<SelectListItem> FactoryDropDownForm = new List<SelectListItem>();

		public string AccountName
		{
			get;
			set;
		}

		public string AccountNo
		{
			get;
			set;
		}

		public string AccountNo2
		{
			get;
			set;
		}

		public string AccountType
		{
			get;
			set;
		}


		

		public List<GFL_Live_Agent_Application> AgentListNew { get; set; }

		public string AgentCode
		{
			get;
			set;
		}

		[Display(Name = "  Agent ")]
		public string AgentFilter
		{
			get;
			set;
		}

	
		public string AgentName
		{
			get;
			set;
		}

		public decimal? Amount
		{
			get;
			set;
		}

		public bool BalanceSMSSent
		{
			get;
			set;
		}

		public string BankCode
		{
			get;
			set;
		}

		public string BankName
		{
			get;
			set;
		}

		public string BankNo
		{
			get;
			set;
		}

		public decimal Charge
		{
			get;
			set;
		}

		public string ColorId
		{
			get;
			set;
		}

		public string Comments
		{
			get;
			set;
		}

		public string Completed
		{
			get;
			set;
		}

		public string ConfirmationCode
		{
			get;
			set;
		}

		public string ConfirmationWord
		{
			get;
			set;
		}

		[Display(Name = "Date From")]
		public string DateFilterFrom
		{
			get;
			set;
		}

		[Display(Name = "Date To")]
		public string DateFilterTo
		{
			get;
			set;
		}

		public DateTime? DateFundsReceived
		{
			get;
			set;
		}

		public DateTime? DateOTTNSent
		{
			get;
			set;
		}

		public DateTime? DatePosted
		{
			get;
			set;
		}

		public DateTime? DateSMSSent
		{
			get;
			set;
		}

		public DateTime? DateTransferredToSacco
		{
			get;
			set;
		}

		public string DepositorTelephoneNumber
		{
			get;
			set;
		}

		public string Description
		{
			get;
			set;
		}

		public string DeviceId
		{
			get;
			set;
		}

		public string DocumentNo
		{
			get;
			set;
		}

		public int EntryNo
		{
			get;
			set;
		}

		public bool FundsReceived
		{
			get;
			set;
		}

		public string FundsSource
		{
			get;
			set;
		}

		public string IdNo
		{
			get;
			set;
		}

		public string Latitude
		{
			get;
			set;
		}

		public string Location
		{
			get;
			set;
		}

		public string Longitude
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		public string Operation
		{
			get;
			set;
		}

		public int? OTTN
		{
			get;
			set;
		}

		public int? OTTNSent
		{
			get;
			set;
		}

		public string OTTNTelephoneNo
		{
			get;
			set;
		}

		public bool Posted
		{
			get;
			set;
		}

		public string ReceiverName
		{
			get;
			set;
		}

		public string ReceiverNationalId
		{
			get;
			set;
		}

		public string ReceiverTelephoneNo
		{
			get;
			set;
		}

		
		public string Society
		{
			get;
			set;
		}

		public string SocietyNo
		{
			get;
			set;
		}

		public string SourceTelephoneNumber
		{
			get;
			set;
		}

		public int? status
		{
			get;
			set;
		}

		public bool statusBool
		{
			get;
			set;
		}

		public string StatusBy
		{
			get;
			set;
		}

		public DateTime? StatusDate
		{
			get;
			set;
		}

		public DateTime? StatusTime
		{
			get;
			set;
		}

		public bool SystemCreatedEntry
		{
			get;
			set;
		}

		public DateTime? TimeFundsReceived
		{
			get;
			set;
		}

		public DateTime? TimeOTTNSent
		{
			get;
			set;
		}

		public DateTime? TimePosted
		{
			get;
			set;
		}

		public DateTime? TimeSMSSent
		{
			get;
			set;
		}

		public DateTime? TimeTransferredToSacco
		{
			get;
			set;
		}

		public string TransactionBy
		{
			get;
			set;
		}

		public DateTime? TransactionDate
		{
			get;
			set;
		}

		public int TransactionID
		{
			get;
			set;
		}

		public DateTime? TransactionTime
		{
			get;
			set;
		}

		public int TransactionType
		{
			get;
			set;
		}

		[Display(Name = "Transaction")]
		public string TransactionTypeFilter
		{
			get;
			set;
		}

		[Display(Name = "Region")]
		public string RegionFilter
		{
			get;
			set;
		}

		[Display(Name = "Factory")]
		public string FactoryFilter
		{
			get;
			set;
		}

		public bool? TransferredToSacco
		{
			get;
			set;
		}

		public string TransferredToSaccoBy
		{
			get;
			set;
		}

		public string factory
		{
			get;
			set;
		}

		public string region
		{
			get;
			set;
		}
		public AgencyTransactionsModels()
		{
		}
	}
}