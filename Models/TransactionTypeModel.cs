﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pesaulipo.Models
{
    public class TransactionTypeModel
    {
        public int Code { get; set; }
        public string Name { get; set; }

        public List<TransactionTypeModel> NewTransList
        {
            get;
            set;
        }
    }
}