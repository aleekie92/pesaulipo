﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pesaulipo.Models
{
	public class ReportsAgentModel
	{
		public int AgentGurantorNominationCount
		{
			get;
			set;
		}

		public int AgentBalanceEnquiryCount
		{
			get;
			set;
		}

		public int AgentLoanApplicationCount
		{
			get;
			set;
		}

		public int AgentLoanRepaymentCount
		{
			get;
			set;
		}

		public int AgentMemberRegistrationCount
		{
			get;
			set;
		}

		public int AgentMinistatementCount
		{
			get;
			set;
		}

		public string AgentName
		{
			get;
			set;
		}

		public int AgentTransactionsCount
		{
			get;
			set;
		}

		public int AgentWithdrawalCount
		{
			get;
			set;
		}

		public ReportsAgentModel()
		{
		}
	}
}