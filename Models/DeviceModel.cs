﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace pesaulipo.Models
{
    public class DeviceModel
    {

        [Required]
        public string Name
        {
            get;
            set;
        }

        [Required]
        public string SerialNo
        {
            get;
            set;
        }


        [Required]
        public string TypeCode
        {
            get;
            set;
        }
       
        public string AssignedTo
        {
            get;
            set;
        }


        public string Region
        {
            get;
            set;
        }

        public string Factory
        {
            get;
            set;
        }

        public bool Enabled
        {
            get;
            set;
        }

        public bool Assigned
        {
            get;
            set;
        }

        public DeviceModel()
        {
        }
    }
}