﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pesaulipo.Models
{
	public class UsersModel
	{
		public string Email
		{
			get;
			set;
		}

		public bool EmailConfirmed
		{
			get;
			set;
		}

		public string Id
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		public string Region
		{
			get;
			set;
		}
		public string Factory
		{
			get;
			set;
		}

		public string UserType
		{
			get;
			set;
		}

		public string AgentCode
		{
			get;
			set;
		}
		public string PhoneNumber
		{
			get;
			set;
		}

		public string Otp
		{
			get;
			set;
		}
		public bool Status
		{
			get;
			set;
		}

		public bool Enebled
		{
			get;
			set;
		}

		public int AccessFailedCount
		{
			get;
			set;
		}
		public string LastSuccessLoginTime
		{
			get;
			set;
		}
		public List<UsersModel> usersModels
		{
			get;
			set;
		}

		public UsersModel()
		{
		}
	}
}