﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;

namespace pesaulipo.Models
{
    public class WebServiceUrl
    {
        
        static string serverUrl = "http://172.16.11.145:7029/GFLTEST/WS/GFL%20Live/Codeunit/AGENCYBANKING";
        static string decripted_username, decripted_passwad, decripteddomain;
       


        public static gflServiceRef.AGENCYBANKING serviceDataObject
        {
            //credentials();

            get
            {
               
                var ws = new gflServiceRef.AGENCYBANKING();
                credentials();
                try
                {
                    var credentials = new NetworkCredential(decripted_username, decripted_passwad, decripteddomain);
                    //  ws.UseDefaultCredentials = true;
                    ws.Url = string.Format(serverUrl);
                    ws.PreAuthenticate = true;
                    ws.Credentials = credentials;
                }
                catch (Exception ex)
                {
                    ex.Data.Clear();
                }
                return ws;
            }
        }
        public static void credentials()
        {
            using (AgencyBankingDBDataContextDataContext db = new AgencyBankingDBDataContextDataContext())
            {

                SaccoSetup dataah = db.SaccoSetups.Where(m => m.Enabled == true && m.SaccoID=="PORTAL").FirstOrDefault();

                if (dataah != null)
                {
                   
                    decripted_username = Security.Decrypt(dataah.soapusername, true);
                    decripted_passwad = Security.Decrypt(dataah.soappassword, true);
                    decripteddomain = Security.Decrypt(dataah.soapdomain, true);

                }
            }
            
        }
    }
 
}