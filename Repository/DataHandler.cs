﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using pesaulipo.Models;

namespace pesaulipo.Repository
{
    public class DataHandler
    {

        private readonly AgencyBankingDBDataContextDataContext _context = new AgencyBankingDBDataContextDataContext();

        private readonly LiveDataDbContextDataContext _context1 = new LiveDataDbContextDataContext();

        List<WebServiceTransactionModel> agentModels;


        public DataHandler()
        {
        }

        public DashboardViewModel DashBoardFunctions(string user, string agentcode, string region, string factory)
        {
            DashboardViewModel dashboardViewModel = new DashboardViewModel();
            RegisterPasswordlessViewModel userr = new RegisterPasswordlessViewModel();
            
            string str = agentcode;

             thisIsMyDataFunction();

            //agentModels.Count();
            
              if (user.Equals("HQ") || user.Equals("Administrator"))
              {
                dashboardViewModel.AgentCount = (
                    from p in this._context1.GFL_Live_Agent_Applications
                    where p.Approval_Status==2
                    select p).Count<GFL_Live_Agent_Application>();

                dashboardViewModel.SaccoTransactionCount = agentModels.Count();
                dashboardViewModel.SaccoTransactionWeeklyCount = agentModels.Where(c => DateTime.Parse(c.transaction_date) > (DateTime?)DateTime.Now.AddDays(-7)).Count();

              
                dashboardViewModel.SaccoTransactionDailyCount = agentModels.Where(c => DateTime.Parse(c.Transaction_time) > (DateTime?)DateTime.Today).Count();

                dashboardViewModel.Transactions20MinsAgo = agentModels.Count((WebServiceTransactionModel e) => (DateTime.Parse(e.Transaction_time) > (DateTime?)DateTime.Now.AddMinutes(-20)) && (DateTime.Parse(e.Transaction_time) < (DateTime?)DateTime.Now));

                dashboardViewModel.Transactions40MinsAgo =   agentModels.Count((WebServiceTransactionModel e) => (DateTime.Parse(e.Transaction_time) > (DateTime?)DateTime.Now.AddMinutes(-40)) && (DateTime.Parse(e.Transaction_time) < (DateTime?)DateTime.Now.AddMinutes(-20)));

                dashboardViewModel.Transactions60MinsAgo = agentModels.Count((WebServiceTransactionModel e) => (DateTime.Parse(e.Transaction_time) > (DateTime?)DateTime.Now.AddMinutes(-60)) && (DateTime.Parse(e.Transaction_time) < (DateTime?)DateTime.Now.AddMinutes(-40)));
               
                dashboardViewModel.Transactions80MinsAgo = agentModels.Count((WebServiceTransactionModel e) => (DateTime.Parse(e.Transaction_time) > (DateTime?)DateTime.Now.AddMinutes(-80)) && (DateTime.Parse(e.Transaction_time) < (DateTime?)DateTime.Now.AddMinutes(-60)));
                
                dashboardViewModel.Transactions100MinsAgo = agentModels.Count((WebServiceTransactionModel e) => (DateTime.Parse(e.Transaction_time) > (DateTime?)DateTime.Now.AddMinutes(-100)) && (DateTime.Parse(e.Transaction_time) < (DateTime?)DateTime.Now.AddMinutes(-80)));
               
                dashboardViewModel.Transactions120MinsAgo = agentModels.Count((WebServiceTransactionModel e) => (DateTime.Parse(e.Transaction_time) > (DateTime?)DateTime.Now.AddMinutes(-120)) && (DateTime.Parse(e.Transaction_time) < (DateTime?)DateTime.Now.AddMinutes(-100)));
               
                dashboardViewModel.Transactions140MinsAgo = agentModels.Count((WebServiceTransactionModel e) => (DateTime.Parse(e.Transaction_time) > (DateTime?)DateTime.Now.AddMinutes(-140)) && (DateTime.Parse(e.Transaction_time) < (DateTime?)DateTime.Now.AddMinutes(-120)));

                dashboardViewModel.Transactions160MinsAgo = agentModels.Count((WebServiceTransactionModel e) => (DateTime.Parse(e.Transaction_time) > (DateTime?)DateTime.Now.AddMinutes(-160)) && (DateTime.Parse(e.Transaction_time) < (DateTime?)DateTime.Now.AddMinutes(-140)));


                dashboardViewModel.Transactions180MinsAgo = agentModels.Count((WebServiceTransactionModel e) => (DateTime.Parse(e.Transaction_time) > (DateTime?)DateTime.Now.AddMinutes(-180)) && (DateTime.Parse(e.Transaction_time) < (DateTime?)DateTime.Now.AddMinutes(-160)));
                dashboardViewModel.Transactions200MinsAgo = agentModels.Count((WebServiceTransactionModel e) => (DateTime.Parse(e.Transaction_time) > (DateTime?)DateTime.Now.AddMinutes(-200)) && (DateTime.Parse(e.Transaction_time) < (DateTime?)DateTime.Now.AddMinutes(-180)));


                dashboardViewModel.MemberRegCount = agentModels.Where(c => DateTime.Parse(c.Transaction_time) > (DateTime?)DateTime.Now.AddMonths(-1) && c.Transaction_type == "MemberRegistration").Count();


                dashboardViewModel.LoanApplicationCount = agentModels.Where(c => DateTime.Parse(c.Transaction_time) > (DateTime?)DateTime.Now.AddMonths(-1) &&  c.Transaction_type == "LoanApplication").Count();


                dashboardViewModel.OthersCount = agentModels.Where(c => DateTime.Parse(c.Transaction_time) > (DateTime?)DateTime.Now.AddMonths(-1) &&  c.Transaction_type != "MemberRegistration" && c.Transaction_type != "LoanApplication").Count();
            }

            else if (user.Equals("Supervisor"))
            {

                dashboardViewModel.DeviceCount = (
                    from p in this._context.Devices
                    where p.Region==region
                    select p).Count<Device>();

                dashboardViewModel.SaccoTransactionCount = agentModels.Where(c => c.global_dimension_filter1==region).Count(); 
                dashboardViewModel.SaccoTransactionWeeklyCount = agentModels.Where(c => DateTime.Parse(c.transaction_date) > (DateTime?)DateTime.Now.AddDays(-7)&& c.global_dimension_filter1==region).Count(); 
                dashboardViewModel.SaccoTransactionDailyCount = agentModels.Where(c => DateTime.Parse(c.Transaction_time) > (DateTime?)DateTime.Today && c.global_dimension_filter1==region).Count();

                dashboardViewModel.Transactions20MinsAgo = agentModels.Count((WebServiceTransactionModel e) => (DateTime.Parse(e.Transaction_time) > (DateTime?)DateTime.Now.AddMinutes(-20)) && e.global_dimension_filter1 == region && (DateTime.Parse(e.Transaction_time) < (DateTime?)DateTime.Now));

                dashboardViewModel.Transactions40MinsAgo = agentModels.Count((WebServiceTransactionModel e) => (DateTime.Parse(e.Transaction_time) > (DateTime?)DateTime.Now.AddMinutes(-40)) && e.global_dimension_filter1 == region && (DateTime.Parse(e.Transaction_time) < (DateTime?)DateTime.Now.AddMinutes(-20)));

                dashboardViewModel.Transactions60MinsAgo = agentModels.Count((WebServiceTransactionModel e) => (DateTime.Parse(e.Transaction_time) > (DateTime?)DateTime.Now.AddMinutes(-60)) && e.global_dimension_filter1 == region  && (DateTime.Parse(e.Transaction_time) < (DateTime?)DateTime.Now.AddMinutes(-40)));

                dashboardViewModel.Transactions80MinsAgo = agentModels.Count((WebServiceTransactionModel e) => (DateTime.Parse(e.Transaction_time) > (DateTime?)DateTime.Now.AddMinutes(-80)) && e.global_dimension_filter1 == region && (DateTime.Parse(e.Transaction_time) < (DateTime?)DateTime.Now.AddMinutes(-60)));

                dashboardViewModel.Transactions100MinsAgo = agentModels.Count((WebServiceTransactionModel e) => (DateTime.Parse(e.Transaction_time) > (DateTime?)DateTime.Now.AddMinutes(-100)) && e.global_dimension_filter1 == region && (DateTime.Parse(e.Transaction_time) < (DateTime?)DateTime.Now.AddMinutes(-80)));

                dashboardViewModel.Transactions120MinsAgo = agentModels.Count((WebServiceTransactionModel e) => (DateTime.Parse(e.Transaction_time) > (DateTime?)DateTime.Now.AddMinutes(-120)) && e.global_dimension_filter1 == region && (DateTime.Parse(e.Transaction_time) < (DateTime?)DateTime.Now.AddMinutes(-100)));

                dashboardViewModel.Transactions140MinsAgo = agentModels.Count((WebServiceTransactionModel e) => (DateTime.Parse(e.Transaction_time) > (DateTime?)DateTime.Now.AddMinutes(-140)) && e.global_dimension_filter1 == region && (DateTime.Parse(e.Transaction_time) < (DateTime?)DateTime.Now.AddMinutes(-120)));

                dashboardViewModel.Transactions160MinsAgo = agentModels.Count((WebServiceTransactionModel e) => (DateTime.Parse(e.Transaction_time) > (DateTime?)DateTime.Now.AddMinutes(-160)) && e.global_dimension_filter1 == region && (DateTime.Parse(e.Transaction_time) < (DateTime?)DateTime.Now.AddMinutes(-140)));


                dashboardViewModel.Transactions180MinsAgo = agentModels.Count((WebServiceTransactionModel e) => (DateTime.Parse(e.Transaction_time) > (DateTime?)DateTime.Now.AddMinutes(-180)) && e.global_dimension_filter1 == region && (DateTime.Parse(e.Transaction_time) < (DateTime?)DateTime.Now.AddMinutes(-160)));
                dashboardViewModel.Transactions200MinsAgo = agentModels.Count((WebServiceTransactionModel e) => (DateTime.Parse(e.Transaction_time) > (DateTime?)DateTime.Now.AddMinutes(-200)) && e.global_dimension_filter1 == region && (DateTime.Parse(e.Transaction_time) < (DateTime?)DateTime.Now.AddMinutes(-180)));

                dashboardViewModel.MemberRegCount = agentModels.Where(c => DateTime.Parse(c.Transaction_time) > (DateTime?)DateTime.Now.AddMonths(-1) && c.global_dimension_filter1 == region && c.Transaction_type=="MemberRegistration").Count(); 
                
              
                dashboardViewModel.LoanApplicationCount = agentModels.Where(c => DateTime.Parse(c.Transaction_time) > (DateTime?)DateTime.Now.AddMonths(-1) && c.global_dimension_filter1 == region && c.Transaction_type == "LoanApplication").Count(); 
                
              
                dashboardViewModel.OthersCount = agentModels.Where(c => DateTime.Parse(c.Transaction_time) > (DateTime?)DateTime.Now.AddMonths(-1) && c.global_dimension_filter1 == region && c.Transaction_type != "MemberRegistration" && c.Transaction_type !="LoanApplication").Count(); 
            }
            else if (user.Equals("CO"))
            {
                dashboardViewModel.DeviceCount = (
                        from p in this._context.Devices
                        where p.Factory==factory
                        select p).Count<Device>();

                dashboardViewModel.SaccoTransactionCount = agentModels.Where(c => c.global_dimension_filter2 == factory).Count();
                dashboardViewModel.SaccoTransactionWeeklyCount = agentModels.Where(c => DateTime.Parse(c.transaction_date) > (DateTime?)DateTime.Now.AddDays(-7) && c.global_dimension_filter2 == factory).Count();
                dashboardViewModel.SaccoTransactionDailyCount = agentModels.Where(c => DateTime.Parse(c.Transaction_time) > (DateTime?)DateTime.Today && c.global_dimension_filter2 == factory).Count();

                dashboardViewModel.Transactions20MinsAgo = agentModels.Count((WebServiceTransactionModel e) => (DateTime.Parse(e.Transaction_time) > (DateTime?)DateTime.Now.AddMinutes(-20)) && e.global_dimension_filter2 == factory && (DateTime.Parse(e.Transaction_time) < (DateTime?)DateTime.Now));

                dashboardViewModel.Transactions40MinsAgo = agentModels.Count((WebServiceTransactionModel e) => (DateTime.Parse(e.Transaction_time) > (DateTime?)DateTime.Now.AddMinutes(-40))  && e.global_dimension_filter2 == factory && (DateTime.Parse(e.Transaction_time) < (DateTime?)DateTime.Now.AddMinutes(-20)));

                dashboardViewModel.Transactions60MinsAgo = agentModels.Count((WebServiceTransactionModel e) => (DateTime.Parse(e.Transaction_time) > (DateTime?)DateTime.Now.AddMinutes(-60)) && e.global_dimension_filter2 == factory && (DateTime.Parse(e.Transaction_time) < (DateTime?)DateTime.Now.AddMinutes(-40)));

                dashboardViewModel.Transactions80MinsAgo = agentModels.Count((WebServiceTransactionModel e) => (DateTime.Parse(e.Transaction_time) > (DateTime?)DateTime.Now.AddMinutes(-80)) && e.global_dimension_filter2 == factory && (DateTime.Parse(e.Transaction_time) < (DateTime?)DateTime.Now.AddMinutes(-60)));

                dashboardViewModel.Transactions100MinsAgo = agentModels.Count((WebServiceTransactionModel e) => (DateTime.Parse(e.Transaction_time) > (DateTime?)DateTime.Now.AddMinutes(-100)) && e.global_dimension_filter2 == factory &&  (DateTime.Parse(e.Transaction_time) < (DateTime?)DateTime.Now.AddMinutes(-80)));

                dashboardViewModel.Transactions120MinsAgo = agentModels.Count((WebServiceTransactionModel e) => (DateTime.Parse(e.Transaction_time) > (DateTime?)DateTime.Now.AddMinutes(-120)) && e.global_dimension_filter2 == factory && (DateTime.Parse(e.Transaction_time) < (DateTime?)DateTime.Now.AddMinutes(-100)));

                dashboardViewModel.Transactions140MinsAgo = agentModels.Count((WebServiceTransactionModel e) => (DateTime.Parse(e.Transaction_time) > (DateTime?)DateTime.Now.AddMinutes(-140)) && e.global_dimension_filter2 == factory && (DateTime.Parse(e.Transaction_time) < (DateTime?)DateTime.Now.AddMinutes(-120)));

                dashboardViewModel.Transactions160MinsAgo = agentModels.Count((WebServiceTransactionModel e) => (DateTime.Parse(e.Transaction_time) > (DateTime?)DateTime.Now.AddMinutes(-160)) && e.global_dimension_filter2 == factory &&  (DateTime.Parse(e.Transaction_time) < (DateTime?)DateTime.Now.AddMinutes(-140)));


                dashboardViewModel.Transactions180MinsAgo = agentModels.Count((WebServiceTransactionModel e) => (DateTime.Parse(e.Transaction_time) > (DateTime?)DateTime.Now.AddMinutes(-180)) && e.global_dimension_filter2 == factory &&  (DateTime.Parse(e.Transaction_time) < (DateTime?)DateTime.Now.AddMinutes(-160)));
                dashboardViewModel.Transactions200MinsAgo = agentModels.Count((WebServiceTransactionModel e) => (DateTime.Parse(e.Transaction_time) > (DateTime?)DateTime.Now.AddMinutes(-200)) && e.global_dimension_filter2 == factory &&  (DateTime.Parse(e.Transaction_time) < (DateTime?)DateTime.Now.AddMinutes(-180)));

                dashboardViewModel.MemberRegCount = agentModels.Where(c => DateTime.Parse(c.Transaction_time) > (DateTime?)DateTime.Now.AddMonths(-1) && c.global_dimension_filter2 == factory && c.Transaction_type == "MemberRegistration").Count();


                dashboardViewModel.LoanApplicationCount = agentModels.Where(c => DateTime.Parse(c.Transaction_time) > (DateTime?)DateTime.Now.AddMonths(-1) && c.global_dimension_filter2 == factory && c.Transaction_type == "LoanApplication").Count();


                dashboardViewModel.OthersCount = agentModels.Where(c => DateTime.Parse(c.Transaction_time) > (DateTime?)DateTime.Now.AddMonths(-1) && c.global_dimension_filter2 == factory && c.Transaction_type != "MemberRegistration" && c.Transaction_type != "LoanApplication").Count();
            }

            else if (user.Equals("Agent"))
            {
                dashboardViewModel.SaccoTransactionCount = agentModels.Where(c => c.agent_code == str).Count();
                dashboardViewModel.SaccoTransactionWeeklyCount = agentModels.Where(c => DateTime.Parse(c.transaction_date) > (DateTime?)DateTime.Now.AddDays(-7) && c.agent_code == str).Count();
                dashboardViewModel.SaccoTransactionDailyCount = agentModels.Where(c => DateTime.Parse(c.Transaction_time) > (DateTime?)DateTime.Today && c.agent_code == str).Count();

                dashboardViewModel.Transactions20MinsAgo = agentModels.Count((WebServiceTransactionModel e) => (DateTime.Parse(e.Transaction_time) > (DateTime?)DateTime.Now.AddMinutes(-20)) && e.agent_code == str && (DateTime.Parse(e.Transaction_time) < (DateTime?)DateTime.Now));

                dashboardViewModel.Transactions40MinsAgo = agentModels.Count((WebServiceTransactionModel e) => (DateTime.Parse(e.Transaction_time) > (DateTime?)DateTime.Now.AddMinutes(-40)) && e.agent_code == str && (DateTime.Parse(e.Transaction_time) < (DateTime?)DateTime.Now.AddMinutes(-20)));

                dashboardViewModel.Transactions60MinsAgo = agentModels.Count((WebServiceTransactionModel e) => (DateTime.Parse(e.Transaction_time) > (DateTime?)DateTime.Now.AddMinutes(-60)) && e.agent_code == str && (DateTime.Parse(e.Transaction_time) < (DateTime?)DateTime.Now.AddMinutes(-40)));

                dashboardViewModel.Transactions80MinsAgo = agentModels.Count((WebServiceTransactionModel e) => (DateTime.Parse(e.Transaction_time) > (DateTime?)DateTime.Now.AddMinutes(-80)) && e.agent_code == str && (DateTime.Parse(e.Transaction_time) < (DateTime?)DateTime.Now.AddMinutes(-60)));

                dashboardViewModel.Transactions100MinsAgo = agentModels.Count((WebServiceTransactionModel e) => (DateTime.Parse(e.Transaction_time) > (DateTime?)DateTime.Now.AddMinutes(-100)) && e.agent_code == str && (DateTime.Parse(e.Transaction_time) < (DateTime?)DateTime.Now.AddMinutes(-80)));

                dashboardViewModel.Transactions120MinsAgo = agentModels.Count((WebServiceTransactionModel e) => (DateTime.Parse(e.Transaction_time) > (DateTime?)DateTime.Now.AddMinutes(-120)) && e.agent_code == str && (DateTime.Parse(e.Transaction_time) < (DateTime?)DateTime.Now.AddMinutes(-100)));

                dashboardViewModel.Transactions140MinsAgo = agentModels.Count((WebServiceTransactionModel e) => (DateTime.Parse(e.Transaction_time) > (DateTime?)DateTime.Now.AddMinutes(-140)) && e.agent_code == str && (DateTime.Parse(e.Transaction_time) < (DateTime?)DateTime.Now.AddMinutes(-120)));

                dashboardViewModel.Transactions160MinsAgo = agentModels.Count((WebServiceTransactionModel e) => (DateTime.Parse(e.Transaction_time) > (DateTime?)DateTime.Now.AddMinutes(-160)) && e.agent_code == str && (DateTime.Parse(e.Transaction_time) < (DateTime?)DateTime.Now.AddMinutes(-140)));


                dashboardViewModel.Transactions180MinsAgo = agentModels.Count((WebServiceTransactionModel e) => (DateTime.Parse(e.Transaction_time) > (DateTime?)DateTime.Now.AddMinutes(-180)) && e.agent_code == str && (DateTime.Parse(e.Transaction_time) < (DateTime?)DateTime.Now.AddMinutes(-160)));
                dashboardViewModel.Transactions200MinsAgo = agentModels.Count((WebServiceTransactionModel e) => (DateTime.Parse(e.Transaction_time) > (DateTime?)DateTime.Now.AddMinutes(-200)) && e.agent_code == str && (DateTime.Parse(e.Transaction_time) < (DateTime?)DateTime.Now.AddMinutes(-180)));

                dashboardViewModel.MemberRegCount = agentModels.Where(c => DateTime.Parse(c.Transaction_time) > (DateTime?)DateTime.Now.AddMonths(-1) && c.agent_code== str  && c.Transaction_type == "MemberRegistration").Count();

                dashboardViewModel.LoanApplicationCount = agentModels.Where(c => DateTime.Parse(c.Transaction_time) > (DateTime?)DateTime.Now.AddMonths(-1) && c.agent_code == str && c.Transaction_type == "LoanApplication").Count();

                dashboardViewModel.OthersCount = agentModels.Where(c => DateTime.Parse(c.Transaction_time) > (DateTime?)DateTime.Now.AddMonths(-1) && c.agent_code == str && c.Transaction_type != "MemberRegistration" && c.Transaction_type != "LoanApplication").Count();

                

            }

            return dashboardViewModel;
        }


        public List<AgentModel> GetSaccoAgentsList(string user, string region, string factory)
        {
            List<AgentModel> agentModels = new List<AgentModel>();

            if (user.Equals("HQ") || user.Equals("Administrator"))
            {
                foreach (GFL_Live_Agent_Application list in (
                    from x in this._context1.GFL_Live_Agent_Applications
                    where x.Approval_Status==2
                    select x).ToList<GFL_Live_Agent_Application>())
                {
                    AgentModel agentModel = new AgentModel()
                    {
                        name = list.Name,
                        AgentCode = list.Agent_Code,
                        DateRegistered = list.Date_Entered,
                        Location = list.Factory_Name,
                        TimeRegistred = list.Time_Entered,
                        Telephone = list.Mobile_No,
                        status=list.Approval_Status,
                        deviceID=list.Device_ID
                        
                    };
                    agentModels.Add(agentModel);
                }
            }

            if (user.Equals("Supervisor"))
            {
                foreach (GFL_Live_Agent_Application list in (
                    from x in this._context1.GFL_Live_Agent_Applications
                    where x.RegionCode==region && x.Approval_Status == 2
                    select x).ToList<GFL_Live_Agent_Application>())
                {
                    AgentModel agentModel = new AgentModel()
                    {
                        name = list.Name,
                        AgentCode = list.Agent_Code,
                        DateRegistered = list.Date_Entered,
                        Location = list.Factory_Name,
                        TimeRegistred = list.Time_Entered,
                        Telephone = list.Mobile_No
                    };
                    agentModels.Add(agentModel);
                }
            }
            if (user.Equals("CO")) 
            {
                foreach (GFL_Live_Agent_Application list in (
                    from x in this._context1.GFL_Live_Agent_Applications
                    where x.Factory_Code==factory && x.Approval_Status == 2
                    select x).ToList<GFL_Live_Agent_Application>())
                {
                    AgentModel agentModel = new AgentModel()
                    {
                        name = list.Name,
                        AgentCode = list.Agent_Code,
                        DateRegistered = list.Date_Entered,
                        Location = list.Factory_Name,
                        TimeRegistred = list.Time_Entered,
                        Telephone = list.Mobile_No
                    };
                    agentModels.Add(agentModel);
                }
            }
          
            return agentModels;
        }


        public List<Region> GetRegions()
        {

            using (AgencyBankingDBDataContextDataContext db = new AgencyBankingDBDataContextDataContext())
            {
                List<Region> model = db.Regions.ToList();

                return model;
            }
        }


        public List<Factory> GetFactories()
        {

            using (AgencyBankingDBDataContextDataContext db = new AgencyBankingDBDataContextDataContext())
            {
                List<Factory> model = db.Factories.ToList();

                return model;
            }
        }
        

        public Reports Reports(string user, DateTime? dateFromPar, DateTime? dateToPar, string transactionTyp, string agentType,string agentcode,string region, string factory)
        {

            Reports report = new Reports();
            thisIsMyDataFunction();

            if (user.Equals("HQ")  || user.Equals("Administrator"))
            {
                List<WebServiceTransactionModel> agencyTransactionsModels1 = new List<WebServiceTransactionModel>();
                
                string str1 = transactionTyp;

                    report.AgentNameList = (
                        from f in this._context1.GFL_Live_Agent_Applications
                        select f).ToList<GFL_Live_Agent_Application>();
                report.ParticularSaccoLoanApplicationCount501000 = agentModels.Where(c => c.Transaction_type=="LoanApplication").Count();
                report.ParticularSaccoLoanApplicationCount10003000 = agentModels.Where(c => c.Transaction_type == "LoanApplication").Count();
                report.ParticularSaccoLoanApplicationCount30006000 = agentModels.Where(c => c.Transaction_type == "LoanApplication").Count();
                report.ParticularSaccoLoanApplicationCount600010000 = agentModels.Where(c => c.Transaction_type == "LoanApplication").Count();
                report.ParticularSaccoLoanApplicationCount1000015000 = agentModels.Where(c => c.Transaction_type == "LoanApplication").Count();
                report.ParticularSaccoLoanApplicationCount1500025000 = agentModels.Where(c => c.Transaction_type == "LoanApplication").Count();
                report.ParticularSaccoLoanApplicationCount2500050000 = agentModels.Where(c => c.Transaction_type == "LoanApplication").Count();
                report.ParticularSaccoLoanApplicationCount50000100000 = agentModels.Where(c => c.Transaction_type == "LoanApplication").Count();

                report.ParticularSaccoMemberactivationCount1001000 = agentModels.Where(c => c.Transaction_type == "Memberactivation"  && Decimal.Parse(c.Amount) > (decimal?)((decimal)99) && Decimal.Parse(c.Amount) < (decimal?)((decimal)1001)).Count();

               
                    report.ParticularSaccoMemberactivationCount10013000 = agentModels.Where(c => c.Transaction_type == "Memberactivation" && Decimal.Parse(c.Amount) > (decimal?)((decimal)1000) && Decimal.Parse(c.Amount) < (decimal?)((decimal)3001)).Count();

                
                    report.ParticularSaccoMemberactivationCount30016000 = agentModels.Where(c => c.Transaction_type == "Memberactivation" && Decimal.Parse(c.Amount) > (decimal?)((decimal)3000) && Decimal.Parse(c.Amount) < (decimal?)((decimal)6001)).Count(); 
                
               
                    report.ParticularSaccoMemberactivationCount600110000 = agentModels.Where(c => c.Transaction_type == "Memberactivation" && Decimal.Parse(c.Amount) > (decimal?)((decimal)6000) && Decimal.Parse(c.Amount) < (decimal?)((decimal)10001)).Count();

             
                    report.ParticularSaccoMemberactivationCount1000015000 = agentModels.Where(c => c.Transaction_type == "Memberactivation" && Decimal.Parse(c.Amount) > (decimal?)((decimal)10000) && Decimal.Parse(c.Amount) < (decimal?)((decimal)15001)).Count();

               
                    report.ParticularSaccoMemberactivationCount1500125000 = agentModels.Where(c => c.Transaction_type == "Memberactivation" && Decimal.Parse(c.Amount) > (decimal?)((decimal)15000) && Decimal.Parse(c.Amount) < (decimal?)((decimal)25001)).Count(); 
                
               
                    report.SaccoAgentsMemberactivationCount = agentModels.Where(c => c.Transaction_type == "Memberactivation").Count();

               
                    
                    report.SaccoAgentsLoanApplicationCount = agentModels.Where(c => c.Transaction_type == "LoanApplication").Count();
                report.SaccoAgentsBalanceEnquiryCount = agentModels.Where(c => c.Transaction_type == "Balance").Count();
                report.SaccoAgentsLoanRepaymentCount = agentModels.Where(c => c.Transaction_type == "LoanRepayment").Count();
                report.SaccoAgentsMinistatementCount = agentModels.Where(c => c.Transaction_type == "Ministatement").Count();

                report.SaccoAgentsMemberRegistrationCount = agentModels.Where(c => c.Transaction_type == "MemberRegistration").Count();
                report.SaccoAgentsGurantorNominationCount = agentModels.Where(c => c.Transaction_type == "GurantorNomination").Count();
                report.SaccoAgentsTransactionsCount = agentModels.Count();


                report.openRegistrationCount = agentModels.Where(c => c.Transaction_type == "MemberRegistration" && c.approval_status == "Open").Count();
                report.pendingRegistrationCount = agentModels.Where(c => c.Transaction_type == "MemberRegistration" && c.approval_status == "Pending").Count();
                report.approvedRegistrationCount = agentModels.Where(c => c.Transaction_type == "MemberRegistration" && c.approval_status == "Approved").Count();
                report.rejectedRegistrationCount = agentModels.Where(c => c.Transaction_type == "MemberRegistration" && c.approval_status == "Rejected").Count();

                report.openLoanApplication = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.approval_status== "Open").Count();
               
                report.pendingLoanApplication = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.approval_status == "Pending").Count();
                report.approvedLoanApplication = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.approval_status == "Approved").Count();
                report.rejectedLoanApplication = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.approval_status == "Rejected").Count();
            }

           
            if (user.Equals("Supervisor"))
            {
                List<AgencyTransactionsModels> agencyTransactionsModels1 = new List<AgencyTransactionsModels>();
                string str1 = transactionTyp;

                report.AgentNameList = (
                        from f in this._context1.GFL_Live_Agent_Applications
                        where f.RegionCode==region
                        select f).ToList<GFL_Live_Agent_Application>();
                report.ParticularSaccoLoanApplicationCount501000 = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.global_dimension_filter1==region).Count();
                report.ParticularSaccoLoanApplicationCount10003000 = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.global_dimension_filter1 == region).Count();
                report.ParticularSaccoLoanApplicationCount30006000 = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.global_dimension_filter1 == region).Count();
                report.ParticularSaccoLoanApplicationCount600010000 = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.global_dimension_filter1 == region).Count();
                report.ParticularSaccoLoanApplicationCount1000015000 = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.global_dimension_filter1 == region).Count();
                report.ParticularSaccoLoanApplicationCount1500025000 = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.global_dimension_filter1 == region).Count();
                report.ParticularSaccoLoanApplicationCount2500050000 = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.global_dimension_filter1 == region).Count();
                report.ParticularSaccoLoanApplicationCount50000100000 = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.global_dimension_filter1 == region).Count();

                report.ParticularSaccoMemberactivationCount1001000 = agentModels.Where(c => c.Transaction_type == "Memberactivation" && c.global_dimension_filter1 == region &&  Decimal.Parse(c.Amount) > (decimal?)((decimal)99) && Decimal.Parse(c.Amount) < (decimal?)((decimal)1001)).Count();


                report.ParticularSaccoMemberactivationCount10013000 = agentModels.Where(c => c.Transaction_type == "Memberactivation" && c.global_dimension_filter1 == region && Decimal.Parse(c.Amount) > (decimal?)((decimal)1000) && Decimal.Parse(c.Amount) < (decimal?)((decimal)3001)).Count();


                report.ParticularSaccoMemberactivationCount30016000 = agentModels.Where(c => c.Transaction_type == "Memberactivation" && c.global_dimension_filter1 == region && Decimal.Parse(c.Amount) > (decimal?)((decimal)3000) && Decimal.Parse(c.Amount) < (decimal?)((decimal)6001)).Count();


                report.ParticularSaccoMemberactivationCount600110000 = agentModels.Where(c => c.Transaction_type == "Memberactivation" && c.global_dimension_filter1 == region  && Decimal.Parse(c.Amount) > (decimal?)((decimal)6000) && Decimal.Parse(c.Amount) < (decimal?)((decimal)10001)).Count();


                report.ParticularSaccoMemberactivationCount1000015000 = agentModels.Where(c => c.Transaction_type == "Memberactivation" && c.global_dimension_filter1 == region && Decimal.Parse(c.Amount) > (decimal?)((decimal)10000) && Decimal.Parse(c.Amount) < (decimal?)((decimal)15001)).Count();


                report.ParticularSaccoMemberactivationCount1500125000 = agentModels.Where(c => c.Transaction_type == "Memberactivation" && c.global_dimension_filter1 == region  && Decimal.Parse(c.Amount) > (decimal?)((decimal)15000) && Decimal.Parse(c.Amount) < (decimal?)((decimal)25001)).Count();


                report.SaccoAgentsMemberactivationCount = agentModels.Where(c => c.Transaction_type == "Memberactivation" && c.global_dimension_filter1 == region).Count();



                report.SaccoAgentsLoanApplicationCount  = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.global_dimension_filter1==region ).Count();
                report.SaccoAgentsBalanceEnquiryCount = agentModels.Where(c => c.Transaction_type == "Balance" && c.global_dimension_filter1 == region).Count();
                report.SaccoAgentsLoanRepaymentCount = agentModels.Where(c => c.Transaction_type == "LoanRepayment" && c.global_dimension_filter1 == region).Count();
                report.SaccoAgentsMinistatementCount = agentModels.Where(c => c.Transaction_type == "Ministatement" && c.global_dimension_filter1 == region).Count();

                report.SaccoAgentsMemberRegistrationCount = agentModels.Where(c => c.Transaction_type == "MemberRegistration" && c.global_dimension_filter1==region).Count();
                report.SaccoAgentsGurantorNominationCount = agentModels.Where(c => c.Transaction_type == "GurantorNomination" && c.global_dimension_filter1 == region).Count();
                report.SaccoAgentsTransactionsCount = agentModels.Where(c=>c.global_dimension_filter1==region).Count();


                report.openRegistrationCount = agentModels.Where(c => c.Transaction_type == "MemberRegistration" && c.approval_status == "Open" && c.global_dimension_filter1 == region).Count();
                report.pendingRegistrationCount = agentModels.Where(c => c.Transaction_type == "MemberRegistration" && c.approval_status == "Pending" && c.global_dimension_filter1 == region).Count();
                report.approvedRegistrationCount = agentModels.Where(c => c.Transaction_type == "MemberRegistration" && c.approval_status == "Approved" && c.global_dimension_filter1 == region).Count();
                report.rejectedRegistrationCount = agentModels.Where(c => c.Transaction_type == "MemberRegistration" && c.approval_status == "Rejected" && c.global_dimension_filter1 == region).Count();

                report.openLoanApplication = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.approval_status == "Open" && c.global_dimension_filter1==region).Count();
                report.pendingLoanApplication = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.approval_status == "Pending" && c.global_dimension_filter1 == region).Count();
                report.approvedLoanApplication = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.approval_status == "Approved" && c.global_dimension_filter1 == region).Count();
                report.rejectedLoanApplication = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.approval_status == "Rejected" && c.global_dimension_filter1 == region).Count();
            }

            if (user.Equals("CO"))
            {
                List<AgencyTransactionsModels> agencyTransactionsModels1 = new List<AgencyTransactionsModels>();
                string str1 = transactionTyp;

                report.AgentNameList = (
                         from f in this._context1.GFL_Live_Agent_Applications
                         where f.Factory_Code == factory
                         select f).ToList<GFL_Live_Agent_Application>();
                report.ParticularSaccoLoanApplicationCount501000 = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.global_dimension_filter2 == factory).Count();
                report.ParticularSaccoLoanApplicationCount10003000 = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.global_dimension_filter2 == factory).Count();
                report.ParticularSaccoLoanApplicationCount30006000 = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.global_dimension_filter2 == factory).Count();
                report.ParticularSaccoLoanApplicationCount600010000 = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.global_dimension_filter1 == factory).Count();
                report.ParticularSaccoLoanApplicationCount1000015000 = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.global_dimension_filter2 == factory).Count();
                report.ParticularSaccoLoanApplicationCount1500025000 = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.global_dimension_filter2 == factory).Count();
                report.ParticularSaccoLoanApplicationCount2500050000 = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.global_dimension_filter2 == factory).Count();
                report.ParticularSaccoLoanApplicationCount50000100000 = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.global_dimension_filter2 == factory).Count();

                report.ParticularSaccoMemberactivationCount1001000 = agentModels.Where(c => c.Transaction_type == "Memberactivation" && c.global_dimension_filter2 == factory && Decimal.Parse(c.Amount) > (decimal?)((decimal)99) && Decimal.Parse(c.Amount) < (decimal?)((decimal)1001)).Count();


                report.ParticularSaccoMemberactivationCount10013000 = agentModels.Where(c => c.Transaction_type == "Memberactivation" && c.global_dimension_filter2 == factory && Decimal.Parse(c.Amount) > (decimal?)((decimal)1000) && Decimal.Parse(c.Amount) < (decimal?)((decimal)3001)).Count();


                report.ParticularSaccoMemberactivationCount30016000 = agentModels.Where(c => c.Transaction_type == "Memberactivation" && c.global_dimension_filter2 == factory && Decimal.Parse(c.Amount) > (decimal?)((decimal)3000) && Decimal.Parse(c.Amount) < (decimal?)((decimal)6001)).Count();


                report.ParticularSaccoMemberactivationCount600110000 = agentModels.Where(c => c.Transaction_type == "Memberactivation" && c.global_dimension_filter2 == factory && Decimal.Parse(c.Amount) > (decimal?)((decimal)6000) && Decimal.Parse(c.Amount) < (decimal?)((decimal)10001)).Count();


                report.ParticularSaccoMemberactivationCount1000015000 = agentModels.Where(c => c.Transaction_type == "Memberactivation" && c.global_dimension_filter2 == factory && Decimal.Parse(c.Amount) > (decimal?)((decimal)10000) && Decimal.Parse(c.Amount) < (decimal?)((decimal)15001)).Count();


                report.ParticularSaccoMemberactivationCount1500125000 = agentModels.Where(c => c.Transaction_type == "Memberactivation" && c.global_dimension_filter2 == factory && Decimal.Parse(c.Amount) > (decimal?)((decimal)15000) && Decimal.Parse(c.Amount) < (decimal?)((decimal)25001)).Count();


                report.SaccoAgentsMemberactivationCount = agentModels.Where(c => c.Transaction_type == "Memberactivation" && c.global_dimension_filter2 == factory).Count();



                report.SaccoAgentsLoanApplicationCount = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.global_dimension_filter2==factory).Count();
                
                report.SaccoAgentsBalanceEnquiryCount = agentModels.Where(c => c.Transaction_type == "Balance" && c.global_dimension_filter2 == factory).Count();
                report.SaccoAgentsLoanRepaymentCount = agentModels.Where(c => c.Transaction_type == "LoanRepayment" && c.global_dimension_filter2 == factory).Count();
                report.SaccoAgentsMinistatementCount = agentModels.Where(c => c.Transaction_type == "Ministatement" && c.global_dimension_filter2 == factory).Count();

                report.SaccoAgentsMemberRegistrationCount = agentModels.Where(c => c.Transaction_type == "MemberRegistration" && c.global_dimension_filter2==factory).Count();
                report.SaccoAgentsGurantorNominationCount = agentModels.Where(c => c.Transaction_type == "GurantorNomination" && c.global_dimension_filter2 == factory).Count();
                report.SaccoAgentsTransactionsCount = agentModels.Where(c => c.global_dimension_filter2 == factory).Count();


                report.openRegistrationCount = agentModels.Where(c => c.Transaction_type == "MemberRegistration" && c.approval_status == "Open" && c.global_dimension_filter2 == factory).Count();
                report.pendingRegistrationCount = agentModels.Where(c => c.Transaction_type == "MemberRegistration" && c.approval_status == "Pending" && c.global_dimension_filter2 == factory).Count();
                report.approvedRegistrationCount = agentModels.Where(c => c.Transaction_type == "MemberRegistration" && c.approval_status == "Approved" && c.global_dimension_filter2 == factory).Count();
                report.rejectedRegistrationCount = agentModels.Where(c => c.Transaction_type == "MemberRegistration" && c.approval_status == "Rejected" && c.global_dimension_filter2 == factory).Count();

                report.openLoanApplication = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.approval_status == "Open" && c.global_dimension_filter2 == factory).Count();
                report.pendingLoanApplication = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.approval_status == "Pending" && c.global_dimension_filter2 == factory).Count();
                report.approvedLoanApplication = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.approval_status == "Approved" && c.global_dimension_filter2 == factory).Count();
                report.rejectedLoanApplication = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.approval_status == "Rejected" && c.global_dimension_filter2 == factory).Count();
            }

            if (user.Equals("Agent"))
            {
                List<AgencyTransactionsModels> agencyTransactionsModels1 = new List<AgencyTransactionsModels>();
                string str1 = transactionTyp;


                report.ParticularSaccoLoanApplicationCount501000 = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.agent_code == agentcode).Count();
                report.ParticularSaccoLoanApplicationCount10003000 = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.agent_code == agentcode).Count();
                report.ParticularSaccoLoanApplicationCount30006000 = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.agent_code == agentcode).Count();
                report.ParticularSaccoLoanApplicationCount600010000 = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.agent_code == agentcode).Count();
                report.ParticularSaccoLoanApplicationCount1000015000 = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.agent_code == agentcode).Count();
                report.ParticularSaccoLoanApplicationCount1500025000 = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.agent_code == agentcode).Count();
                report.ParticularSaccoLoanApplicationCount2500050000 = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.agent_code == agentcode).Count();
                report.ParticularSaccoLoanApplicationCount50000100000 = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.agent_code == agentcode).Count();

                report.ParticularSaccoMemberactivationCount1001000 = agentModels.Where(c => c.Transaction_type == "Memberactivation" && c.agent_code == agentcode && Decimal.Parse(c.Amount) > (decimal?)((decimal)99) && Decimal.Parse(c.Amount) < (decimal?)((decimal)1001)).Count();


                report.ParticularSaccoMemberactivationCount10013000 = agentModels.Where(c => c.Transaction_type == "Memberactivation" && c.agent_code == agentcode && Decimal.Parse(c.Amount) > (decimal?)((decimal)1000) && Decimal.Parse(c.Amount) < (decimal?)((decimal)3001)).Count();


                report.ParticularSaccoMemberactivationCount30016000 = agentModels.Where(c => c.Transaction_type == "Memberactivation" && c.agent_code == agentcode && Decimal.Parse(c.Amount) > (decimal?)((decimal)3000) && Decimal.Parse(c.Amount) < (decimal?)((decimal)6001)).Count();


                report.ParticularSaccoMemberactivationCount600110000 = agentModels.Where(c => c.Transaction_type == "Memberactivation" && c.agent_code == agentcode && Decimal.Parse(c.Amount) > (decimal?)((decimal)6000) && Decimal.Parse(c.Amount) < (decimal?)((decimal)10001)).Count();


                report.ParticularSaccoMemberactivationCount1000015000 = agentModels.Where(c => c.Transaction_type == "Memberactivation" && c.agent_code == agentcode && Decimal.Parse(c.Amount) > (decimal?)((decimal)10000) && Decimal.Parse(c.Amount) < (decimal?)((decimal)15001)).Count();


                report.ParticularSaccoMemberactivationCount1500125000 = agentModels.Where(c => c.Transaction_type == "Memberactivation" && c.agent_code == agentcode && Decimal.Parse(c.Amount) > (decimal?)((decimal)15000) && Decimal.Parse(c.Amount) < (decimal?)((decimal)25001)).Count();


                report.SaccoAgentsMemberactivationCount = agentModels.Where(c => c.Transaction_type == "Memberactivation" && c.agent_code == agentcode).Count();

                report.SaccoAgentsLoanApplicationCount = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.agent_code==agentcode).Count();
                report.SaccoAgentsBalanceEnquiryCount = agentModels.Where(c => c.Transaction_type == "Balance" && c.agent_code == agentcode).Count();
                report.SaccoAgentsLoanRepaymentCount = agentModels.Where(c => c.Transaction_type == "LoanRepayment" && c.agent_code == agentcode).Count();
                report.SaccoAgentsMinistatementCount = agentModels.Where(c => c.Transaction_type == "Ministatement" && c.agent_code == agentcode).Count();

                report.SaccoAgentsMemberRegistrationCount = agentModels.Where(c => c.Transaction_type == "MemberRegistration" && c.agent_code==agentcode).Count();
                report.SaccoAgentsGurantorNominationCount = agentModels.Where(c => c.Transaction_type == "GurantorNomination" && c.agent_code == agentcode).Count();
                report.SaccoAgentsTransactionsCount = agentModels.Where(c => c.agent_code == agentcode).Count();


                report.openRegistrationCount = agentModels.Where(c => c.Transaction_type == "MemberRegistration" && c.approval_status == "Open" && c.agent_code == agentcode).Count();
                report.pendingRegistrationCount = agentModels.Where(c => c.Transaction_type == "MemberRegistration" && c.approval_status == "Pending" && c.agent_code == agentcode).Count();
                report.approvedRegistrationCount = agentModels.Where(c => c.Transaction_type == "MemberRegistration" && c.approval_status == "Approved" && c.agent_code == agentcode).Count();
                report.rejectedRegistrationCount = agentModels.Where(c => c.Transaction_type == "MemberRegistration" && c.approval_status == "Rejected" && c.agent_code == agentcode).Count();

                report.openLoanApplication = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.approval_status == "Open" && c.agent_code == agentcode).Count();
                report.pendingLoanApplication = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.approval_status == "Pending" && c.agent_code == agentcode).Count();
                report.approvedLoanApplication = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.approval_status == "Approved" && c.agent_code == agentcode).Count();
                report.rejectedLoanApplication = agentModels.Where(c => c.Transaction_type == "LoanApplication" && c.approval_status == "Rejected" && c.agent_code == agentcode).Count();
            }
            return report;
        }

        public List<WebServiceTransactionModel> GetAllAgencyTransactions(DateTime? dateFromPar, DateTime? dateToPar, string transactionTyp,  string agentType, string regionFilter, string factoryFilter, string user, string agentcode, string region, string factory, string statusfil)
        {

            DateTime value;
            DateTime dateTime;
            thisIsMyDataFunction();

            List<WebServiceTransactionModel> agencyTransactions = new List<WebServiceTransactionModel>();
            
            string str = transactionTyp;
            if (user.Equals("HQ") || user.Equals("Administrator"))
            {
                if (!dateFromPar.HasValue)
                {
                    if (dateToPar.HasValue)
                    {
                        value = dateToPar.Value;
                        agencyTransactions = agentModels.Where(c => DateTime.Parse(c.Transaction_time) < value.AddHours(12)).ToList();


                    }
                    else
                    {
                        agencyTransactions = agentModels.ToList();
                    }
                }

                if (dateFromPar.HasValue) {
                    dateTime = dateFromPar.Value;
                    agencyTransactions = agentModels.Where(c => DateTime.Parse(c.Transaction_time) > (DateTime?)dateTime).ToList();
                }
                if (dateToPar.HasValue)
                {
                    dateTime = dateFromPar.Value;
                    value = dateToPar.Value;
                    agencyTransactions = agentModels.Where(c => DateTime.Parse(c.Transaction_time) > (DateTime?)dateTime && DateTime.Parse(c.transaction_date) < (DateTime?)value.AddHours(12)).ToList();

                }

                if (dateFromPar.HasValue && str != null && str != "All")
                {
                    dateTime = dateFromPar.Value;
                    agencyTransactions = agentModels.Where(c => DateTime.Parse(c.Transaction_time) > (DateTime?)dateTime && c.Transaction_type == str).ToList();
                }

                if (!dateFromPar.HasValue && str != null && str != "All")
                {
                    agencyTransactions = agentModels.Where(c => c.Transaction_type == str).ToList();
                }

                if (agentType != null && agentType != "All")
                {
                    //dateTime = dateFromPar.Value;
                    agencyTransactions = agentModels.Where(c => c.agent_code == agentType).ToList();
                }


                if (statusfil != null && statusfil != "All")
                {
                    //dateTime = dateFromPar.Value;
                    agencyTransactions = agentModels.Where(c => c.approval_status == statusfil).ToList();
                }

                if (str !=null && statusfil != null && statusfil != "All")
                {
                    //dateTime = dateFromPar.Value;
                    agencyTransactions = agentModels.Where(c => c.approval_status == statusfil && c.Transaction_type==str).ToList();
                }


                if (regionFilter != null && regionFilter != "Select Region")
                {
                    agencyTransactions = agentModels.Where(c =>c.global_dimension_filter1 == regionFilter).ToList();
                }

                if (factoryFilter != null && factoryFilter != "All")
                {
                    agencyTransactions = agentModels.Where(c =>  c.global_dimension_filter2 == factoryFilter).ToList();
                }

                if (dateFromPar.HasValue && regionFilter != null && regionFilter != "Select Region")
                {
                    dateTime = dateFromPar.Value;
                    agencyTransactions = agentModels.Where(c => DateTime.Parse(c.Transaction_time) > (DateTime?)dateTime && c.global_dimension_filter1 == regionFilter).ToList();
                }

               
            }

            if (user.Equals("Supervisor"))
            {


                if (!dateFromPar.HasValue)
                {
                    if (dateToPar.HasValue)
                    {
                        value = dateToPar.Value;
                        agencyTransactions = agentModels.Where(c => DateTime.Parse(c.Transaction_time) < value.AddHours(12) && c.global_dimension_filter1==region).ToList();


                    }
                    else
                    {
                        agencyTransactions = agentModels.Where(c=>c.global_dimension_filter1 == region) .ToList();
                    }
                }

                if (dateFromPar.HasValue)
                {
                    dateTime = dateFromPar.Value;
                    agencyTransactions = agentModels.Where(c => DateTime.Parse(c.Transaction_time) > (DateTime?)dateTime && c.global_dimension_filter1 == region).ToList();
                }
                if (dateToPar.HasValue)
                {
                    dateTime = dateFromPar.Value;
                    value = dateToPar.Value;
                    agencyTransactions = agentModels.Where(c => DateTime.Parse(c.Transaction_time) > (DateTime?)dateTime && DateTime.Parse(c.transaction_date) < (DateTime?)value.AddHours(12) && c.global_dimension_filter1 == region).ToList();

                }

                if (dateFromPar.HasValue && str != null && str != "All")
                {
                    dateTime = dateFromPar.Value;
                    agencyTransactions = agentModels.Where(c => DateTime.Parse(c.Transaction_time) > (DateTime?)dateTime && c.Transaction_type == str && c.global_dimension_filter1 == region).ToList();
                }

              

                if (str != null && str != "All")
                {
                    agencyTransactions = agentModels.Where(c => c.Transaction_type == str && c.global_dimension_filter1 == region).ToList();
                }

                if (dateFromPar.HasValue && agentType != null && agentType != "All")
                {
                    dateTime = dateFromPar.Value;
                    agencyTransactions = agentModels.Where(c => DateTime.Parse(c.Transaction_time) > (DateTime?)dateTime && c.agent_code == agentType && c.global_dimension_filter1 == region).ToList();
                }

                if (agentType != null && agentType != "All")
                {
                    //dateTime = dateFromPar.Value;
                    agencyTransactions = agentModels.Where(c =>c.agent_code == agentType && c.global_dimension_filter1 == region).ToList();
                }

                if (statusfil != null && statusfil != "All")
                {
                    //dateTime = dateFromPar.Value;
                    agencyTransactions = agentModels.Where(c => c.approval_status == statusfil && c.global_dimension_filter1 == region).ToList();
                }
                if (str != null && statusfil != null && statusfil != "All")
                {
                    //dateTime = dateFromPar.Value;
                    agencyTransactions = agentModels.Where(c => c.approval_status == statusfil && c.global_dimension_filter1 == region && c.Transaction_type==str).ToList();
                }

                if (factoryFilter != null && factoryFilter != "All")
                {
                    
                    agencyTransactions = agentModels.Where(c => c.global_dimension_filter2 == factoryFilter && c.global_dimension_filter1==region).ToList();
                }

            }

            if (user.Equals("CO"))
            {

                if (!dateFromPar.HasValue)
                {
                    if (dateToPar.HasValue)
                    {
                        value = dateToPar.Value;
                        agencyTransactions = agentModels.Where(c => DateTime.Parse(c.Transaction_time) < value.AddHours(12) && c.global_dimension_filter2 == factory).ToList();
                    }
                    else
                    {
                        agencyTransactions = agentModels.Where(c => c.global_dimension_filter2 == factory).ToList();
                    }
                }

                if (dateFromPar.HasValue)
                {
                    dateTime = dateFromPar.Value;
                    agencyTransactions = agentModels.Where(c => DateTime.Parse(c.Transaction_time) > (DateTime?)dateTime && c.global_dimension_filter2 == factory).ToList();
                }
                if (dateToPar.HasValue)
                {
                    dateTime = dateFromPar.Value;
                    value = dateToPar.Value;
                    agencyTransactions = agentModels.Where(c => DateTime.Parse(c.Transaction_time) > (DateTime?)dateTime && DateTime.Parse(c.transaction_date) < (DateTime?)value.AddHours(12) && c.global_dimension_filter2 == factory).ToList();

                }

                if (dateFromPar.HasValue && str != null && str != "All")
                {
                    dateTime = dateFromPar.Value;
                    agencyTransactions = agentModels.Where(c => DateTime.Parse(c.Transaction_time) > (DateTime?)dateTime && c.Transaction_type == str && c.global_dimension_filter2 == factory).ToList();
                }

                if (str != null && str != "All")
                {
                    agencyTransactions = agentModels.Where(c => c.Transaction_type == str && c.global_dimension_filter2 == factory).ToList();
                }

                if (dateFromPar.HasValue && agentType != null && agentType != "All")
                {
                    dateTime = dateFromPar.Value;
                    agencyTransactions = agentModels.Where(c => DateTime.Parse(c.Transaction_time) > (DateTime?)dateTime && c.agent_code == agentType && c.global_dimension_filter2 == factory).ToList();
                }

                if (agentType != null && agentType != "All")
                {
                    agencyTransactions = agentModels.Where(c => c.agent_code == agentType && c.global_dimension_filter2 == factory).ToList();
                }

                if (statusfil != null && statusfil != "All")
                {
                    agencyTransactions = agentModels.Where(c => c.approval_status == statusfil && c.global_dimension_filter2 == factory).ToList();
                }

                if (str != null && statusfil != null && statusfil != "All")
                {
                    //dateTime = dateFromPar.Value;
                    agencyTransactions = agentModels.Where(c => c.approval_status == statusfil && c.global_dimension_filter2 == factory && c.Transaction_type == str).ToList();
                }


            }

            if (user.Equals("Agent"))
            {
                if (!dateFromPar.HasValue)
                {
                    if (dateToPar.HasValue)
                    {
                        value = dateToPar.Value;
                        agencyTransactions = agentModels.Where(c => DateTime.Parse(c.Transaction_time) < value.AddHours(12) && c.agent_code == agentcode).ToList();


                    }
                    else
                    {
                        agencyTransactions = agentModels.Where(c => c.agent_code == agentcode).ToList();
                    }
                }

                if (dateFromPar.HasValue)
                {
                    dateTime = dateFromPar.Value;
                    agencyTransactions = agentModels.Where(c => DateTime.Parse(c.Transaction_time) > (DateTime?)dateTime && c.agent_code == agentcode).ToList();
                }
                if (dateToPar.HasValue)
                {
                    dateTime = dateFromPar.Value;
                    value = dateToPar.Value;
                    agencyTransactions = agentModels.Where(c => DateTime.Parse(c.Transaction_time) > (DateTime?)dateTime && DateTime.Parse(c.transaction_date) < (DateTime?)value.AddHours(12) && c.agent_code == agentcode).ToList();

                }

                if (dateFromPar.HasValue && str != null && str != "All")
                {
                    dateTime = dateFromPar.Value;
                    agencyTransactions = agentModels.Where(c => DateTime.Parse(c.Transaction_time) > (DateTime?)dateTime && c.Transaction_type == str && c.agent_code == agentcode).ToList();
                }

               
                if (str != null && str != "All")
                {
                    agencyTransactions = agentModels.Where(c => c.Transaction_type == str && c.agent_code == agentcode).ToList();
                }

                if (statusfil != null && statusfil != "All")
                {
                    agencyTransactions = agentModels.Where(c => c.approval_status == statusfil && c.agent_code == agentcode).ToList();
                }

                if (str != null && statusfil != null && statusfil != "All")
                {
                    //dateTime = dateFromPar.Value;
                    agencyTransactions = agentModels.Where(c => c.approval_status == statusfil && c.agent_code == agentcode && c.Transaction_type == str).ToList();
                }
            }
            return agencyTransactions;


        }

        public UsersModel ViewAllUsers(string agcode)
        {
            UsersModel usersModel = new UsersModel();
            List<UsersModel> usersModels = new List<UsersModel>();
            List<AspNetUser> aspNetUsers = new List<AspNetUser>();
            aspNetUsers = ( (
                from c in this._context.AspNetUsers
                where c.AgentCode != agcode
                select c).ToList<AspNetUser>());
            foreach (AspNetUser aspNetUser in aspNetUsers)
            {
                UsersModel usersModel1 = new UsersModel()
                {
                    Id = aspNetUser.Id,
                    Name = aspNetUser.UserName,
                    Email = aspNetUser.Email,
                    Region = aspNetUser.Region,
                    PhoneNumber = aspNetUser.PhoneNumber,
                    Factory=aspNetUser.Factory,
                    UserType=aspNetUser.UserType,
                    AgentCode=aspNetUser.AgentCode,
                    EmailConfirmed = aspNetUser.EmailConfirmed,
                    Status=aspNetUser.Status,
                    LastSuccessLoginTime=aspNetUser.LastSuccessLoginTime,
                    AccessFailedCount=aspNetUser.AccessFailedCount,
                    Enebled=aspNetUser.Enabled
                };
                usersModels.Add(usersModel1);
            }
            usersModel.usersModels = usersModels;
            return usersModel;
        }

        public bool DeleteUser(string x)
        {
            bool flag;
            AspNetUser aspNetUser = this._context.AspNetUsers.FirstOrDefault<AspNetUser>((AspNetUser c) => c.Id == x);
            if (aspNetUser == null)
            {
                return false;
            }
            try
            {
                this._context.AspNetUsers.DeleteOnSubmit(aspNetUser);
                this._context.SubmitChanges();
                flag = true;
            }
            catch (Exception exception)
            {
                flag = false;
            }
            return flag;
        }

        public void thisIsMyDataFunction()
        {
            agentModels = new List<WebServiceTransactionModel>();
            string wholearray = WebServiceUrl.serviceDataObject.PortalGetALLTransactions(DateTime.Now.AddDays(-30), DateTime.Now);
            string[] transactions = wholearray.Split(new string[] { ":::" }, StringSplitOptions.RemoveEmptyEntries);

            foreach (var tr in transactions)
            {
                string[] rowvalues = tr.Split('^');

                WebServiceTransactionModel agentModel = new WebServiceTransactionModel()
                {
                    Document_no = rowvalues[0],
                    transaction_date = rowvalues[1],
                    account_no = rowvalues[2],
                    Description = rowvalues[3],
                    Amount = rowvalues[4],
                    Transaction_type = rowvalues[5],
                    Transaction_time = rowvalues[6],
                    date_posted = rowvalues[7],
                    time_posted = rowvalues[8],//9
                    account_status = rowvalues[9],
                    changed = rowvalues[10],
                    date_changed = rowvalues[11],
                    time_changed = rowvalues[12],//13
                    changed_by = rowvalues[13],
                    approved_by = rowvalues[14],//15
                    branch_code = rowvalues[15],
                    activity_code = rowvalues[16],
                    global_dimension_filter1 = rowvalues[17],////regioncode
                    global_dimension_filter2 = rowvalues[18],/////factorycode                      
                    agent_code = rowvalues[19],
                    agent_name = rowvalues[20],
                    loan_no = rowvalues[21],
                    acc_name = rowvalues[22],
                    telephone = rowvalues[23],
                    id_no = rowvalues[24],
                    grower_no = rowvalues[25],
                    approval_status = rowvalues[26],
                    approved_date = rowvalues[27],
                    approved_time = rowvalues[28],

                    device_id = rowvalues[29],
                    gender = rowvalues[30],
                    dob = rowvalues[31],
                    loan_product = rowvalues[32],
                    repayment_period = rowvalues[33],

                    maximum_qualified = rowvalues[34],
                    qualified_kg = rowvalues[35],
                    unfinished_loan = rowvalues[36],
                    loan_product_name = rowvalues[37],
                    amount_to_guarantee = rowvalues[38],
                    mode_of_disbursment = rowvalues[39],

                    bank_code = rowvalues[40],
                    bank_name = rowvalues[41],
                    bank_acc_no = rowvalues[42],
                    bank_branch = rowvalues[43],
                    laon_installments = rowvalues[44],
                    citizenship = rowvalues[45],
                    register_pu = rowvalues[46],
                    next_of_kin = rowvalues[47],
                    relationship_next_of_kin = rowvalues[48],
                    next_of_kin_id = rowvalues[49],
                    guarantor_grower_no = rowvalues[50],
                    applicant_net_amount = rowvalues[51],
                    pu_registration_language = rowvalues[52],
                    sacco_code = rowvalues[53],
                    sacco_acc_no = rowvalues[54],
                };
                agentModels.Add(agentModel);
            }

            //end here..........................................


            //test.........................................

            //for (int tr = 0; tr <= 10; tr++)
            //{
            //    WebServiceTransactionModel agentModel = new WebServiceTransactionModel()
            //    {
            //        Document_no = "APOS208891",
            //        transaction_date = "07/06/20 15:25",
            //        account_no = "N/A",
            //        Description = "Ministatement",
            //        Amount = "0",
            //        Transaction_type = "Ministatement",
            //        Transaction_time = "07/06/20 15:25",
            //        date_posted = "06/03/20",
            //        time_posted = "8:55:55 AM",//9
            //        account_status = "Open",
            //        changed = "No",
            //        date_changed = "06/03/20",
            //        time_changed = "8:55:52 AM",//13
            //        changed_by = "GFL0007",
            //        approved_by = "KTDATEAS\\CKIDEMY",//15
            //        branch_code = "Nairobi",
            //        activity_code = "ACT45",
            //        global_dimension_filter1 = "R3",////regioncode
            //        global_dimension_filter2 = "KN",/////factorycode                      
            //        agent_code = "GFL00089",
            //        agent_name = "MARY NYABOKE KEBWARO",
            //        loan_no = "N/A",
            //        acc_name = "MARYROSE  T  OTERO",
            //        telephone = "+254728017495",
            //        id_no = "27360510",
            //        grower_no = "SA0220030",
            //        approval_status = "Open",
            //        approved_date = "06/04/20",
            //        approved_time = "11:16:46 AM",

            //        device_id = "WP19201Q10000315",
            //        gender = "Male",
            //        dob = "Wednesday, June 3, 2020",
            //        loan_product = "N/A",
            //        repayment_period = "0",

            //        maximum_qualified = "0",
            //        qualified_kg = "0",
            //        unfinished_loan = "NO",
            //        loan_product_name = "N/A",
            //        amount_to_guarantee = "0",
            //        mode_of_disbursment = "N/A",

            //        bank_code = "N/A",
            //        bank_name = "N/A",
            //        bank_acc_no = "N/A",
            //        bank_branch = "N/A",
            //        laon_installments = "0",
            //        citizenship = "N/A",
            //        register_pu = "YES",
            //        next_of_kin = "Yuvenalis Oteri Onguso",
            //        relationship_next_of_kin = "Husband",
            //        next_of_kin_id = "6936370",
            //        guarantor_grower_no = "N/A",
            //        applicant_net_amount = "0",
            //        pu_registration_language = "EN",
            //        sacco_code = "N/A",
            //        sacco_acc_no = "N/A",
            //    };
            //    agentModels.Add(agentModel);
            //}


            //for (int tr = 0; tr <= 10; tr++)
            //{
            //    WebServiceTransactionModel agentModel = new WebServiceTransactionModel()
            //    {
            //        Document_no = "APOS208891",
            //        transaction_date = "06/06/20 07:40",
            //        account_no = "N/A",
            //        Description = "LoanApplication",
            //        Amount = "0",
            //        Transaction_type = "LoanApplication",
            //        Transaction_time = "06/06/20 07:40",
            //        date_posted = "06/03/20",
            //        time_posted = "8:55:55 AM",//9
            //        account_status = "Open",
            //        changed = "No",
            //        date_changed = "06/03/20",
            //        time_changed = "8:55:52 AM",//13
            //        changed_by = "GFL0007",
            //        approved_by = "KTDATEAS\\CKIDEMY",//15
            //        branch_code = "Nairobi",
            //        activity_code = "ACT45",
            //        global_dimension_filter1 = "R3",////regioncode
            //        global_dimension_filter2 = "KN",/////factorycode                      
            //        agent_code = "GFL00089",
            //        agent_name = "MARY NYABOKE KEBWARO",
            //        loan_no = "N/A",
            //        acc_name = "MARYROSE  T  OTERO",
            //        telephone = "+254728017495",
            //        id_no = "27360510",
            //        grower_no = "SA0220030",
            //        approval_status = "Open",
            //        approved_date = "06/04/20",
            //        approved_time = "11:16:46 AM",

            //        device_id = "WP19201Q10000315",
            //        gender = "Male",
            //        dob = "Wednesday, June 3, 2020",
            //        loan_product = "N/A",
            //        repayment_period = "0",

            //        maximum_qualified = "0",
            //        qualified_kg = "0",
            //        unfinished_loan = "NO",
            //        loan_product_name = "N/A",
            //        amount_to_guarantee = "0",
            //        mode_of_disbursment = "N/A",

            //        bank_code = "N/A",
            //        bank_name = "N/A",
            //        bank_acc_no = "N/A",
            //        bank_branch = "N/A",
            //        laon_installments = "0",
            //        citizenship = "N/A",
            //        register_pu = "YES",
            //        next_of_kin = "Yuvenalis Oteri Onguso",
            //        relationship_next_of_kin = "Husband",
            //        next_of_kin_id = "6936370",
            //        guarantor_grower_no = "N/A",
            //        applicant_net_amount = "0",
            //        pu_registration_language = "EN",
            //        sacco_code = "N/A",
            //        sacco_acc_no = "N/A",
            //    };
            //    agentModels.Add(agentModel);
            //}



            //for (int tr = 0; tr <= 10; tr++)
            //{
            //    WebServiceTransactionModel agentModel = new WebServiceTransactionModel()
            //    {
            //        Document_no = "APOS208891",
            //        transaction_date = "06/21/20 15:25",
            //        account_no = "N/A",
            //        Description = "Balance",
            //        Amount = "0",
            //        Transaction_type = "Balance",
            //        Transaction_time = "06/21/20 15:25",
            //        date_posted = "06/03/20",
            //        time_posted = "8:55:55 AM",//9
            //        account_status = "Open",
            //        changed = "No",
            //        date_changed = "06/03/20",
            //        time_changed = "8:55:52 AM",//13
            //        changed_by = "GFL0007",
            //        approved_by = "KTDATEAS\\CKIDEMY",//15
            //        branch_code = "Nairobi",
            //        activity_code = "ACT45",
            //        global_dimension_filter1 = "R3",////regioncode
            //        global_dimension_filter2 = "KT",/////factorycode                      
            //        agent_code = "GFL0137",
            //        agent_name = "MARY NYABOKE KEBWARO",
            //        loan_no = "N/A",
            //        acc_name = "MARYROSE  T  OTERO",
            //        telephone = "+254728017495",
            //        id_no = "27360510",
            //        grower_no = "SA0220030",
            //        approval_status = "Open",
            //        approved_date = "06/04/20",
            //        approved_time = "11:16:46 AM",

            //        device_id = "WP19201Q10000315",
            //        gender = "Male",
            //        dob = "Wednesday, June 3, 2020",
            //        loan_product = "N/A",
            //        repayment_period = "0",

            //        maximum_qualified = "0",
            //        qualified_kg = "0",
            //        unfinished_loan = "NO",
            //        loan_product_name = "N/A",
            //        amount_to_guarantee = "0",
            //        mode_of_disbursment = "N/A",

            //        bank_code = "N/A",
            //        bank_name = "N/A",
            //        bank_acc_no = "N/A",
            //        bank_branch = "N/A",
            //        laon_installments = "0",
            //        citizenship = "N/A",
            //        register_pu = "YES",
            //        next_of_kin = "Yuvenalis Oteri Onguso",
            //        relationship_next_of_kin = "Husband",
            //        next_of_kin_id = "6936370",
            //        guarantor_grower_no = "N/A",
            //        applicant_net_amount = "0",
            //        pu_registration_language = "EN",
            //        sacco_code = "N/A",
            //        sacco_acc_no = "N/A",
            //    };
            //    agentModels.Add(agentModel);
            //}


            //for (int tr = 0; tr <= 10; tr++)
            //{
            //    WebServiceTransactionModel agentModel = new WebServiceTransactionModel()
            //    {
            //        Document_no = "APOS208891",
            //        transaction_date = "06/14/20 15:25",
            //        account_no = "N/A",
            //        Description = "Ministatement",
            //        Amount = "0",
            //        Transaction_type = "Ministatement",
            //        Transaction_time = "06/14/20 15:25",
            //        date_posted = "06/03/20",
            //        time_posted = "8:55:55 AM",//9
            //        account_status = "Open",
            //        changed = "No",
            //        date_changed = "06/03/20",
            //        time_changed = "8:55:52 AM",//13
            //        changed_by = "GFL0007",
            //        approved_by = "KTDATEAS\\CKIDEMY",//15
            //        branch_code = "Nairobi",
            //        activity_code = "ACT45",
            //        global_dimension_filter1 = "R3",////regioncode
            //        global_dimension_filter2 = "CH",/////factorycode                      
            //        agent_code = "GFL0135",
            //        agent_name = "MARY NYABOKE KEBWARO",
            //        loan_no = "N/A",
            //        acc_name = "MARYROSE  T  OTERO",
            //        telephone = "+254728017495",
            //        id_no = "27360510",
            //        grower_no = "SA0220030",
            //        approval_status = "Open",
            //        approved_date = "06/04/20",
            //        approved_time = "11:16:46 AM",

            //        device_id = "WP19201Q10000315",
            //        gender = "Male",
            //        dob = "Wednesday, June 3, 2020",
            //        loan_product = "N/A",
            //        repayment_period = "0",

            //        maximum_qualified = "0",
            //        qualified_kg = "0",
            //        unfinished_loan = "NO",
            //        loan_product_name = "N/A",
            //        amount_to_guarantee = "0",
            //        mode_of_disbursment = "N/A",

            //        bank_code = "N/A",
            //        bank_name = "N/A",
            //        bank_acc_no = "N/A",
            //        bank_branch = "N/A",
            //        laon_installments = "0",
            //        citizenship = "N/A",
            //        register_pu = "YES",
            //        next_of_kin = "Yuvenalis Oteri Onguso",
            //        relationship_next_of_kin = "Husband",
            //        next_of_kin_id = "6936370",
            //        guarantor_grower_no = "N/A",
            //        applicant_net_amount = "0",
            //        pu_registration_language = "EN",
            //        sacco_code = "N/A",
            //        sacco_acc_no = "N/A",
            //    };
            //    agentModels.Add(agentModel);
            //}

            //end here................................
        }
    }
}