﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace pesaulipo
{
	public static class HMTLHelperExtensions
	{
		public static string IsSelected(this HtmlHelper html, string controller = null, string action = null, string cssClass = null)
		{
			if (string.IsNullOrEmpty(cssClass))
			{
				cssClass = "active";
			}
			string item = (string)html.ViewContext.RouteData.Values["action"];
			string str = (string)html.ViewContext.RouteData.Values["controller"];
			if (string.IsNullOrEmpty(controller))
			{
				controller = str;
			}
			if (string.IsNullOrEmpty(action))
			{
				action = item;
			}
			if (controller == str && action == item)
			{
				return cssClass;
			}
			return string.Empty;
		}

		public static string PageClass(this HtmlHelper html)
		{
			return (string)html.ViewContext.RouteData.Values["action"];
		}
	}
}