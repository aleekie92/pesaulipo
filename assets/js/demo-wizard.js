(function ($) {

    $("#wizard2").steps({
        headerTag: "h3",
        bodyTag: "fieldset"
    });

    var form = $("#wizard1").show();

    form.steps({
        headerTag: "h3",
        bodyTag: "fieldset",
        onFinished: function (event, currentIndex) {
            swal({ title: 'Are you sure?', showCancelButton: true }).then(result => {
                if (result.value) {
                    // handle Confirm button click
                    document.getElementById("wizard1").submit();                    
                    // result.value will contain `true` or the input value
                } else {
                    swal("Failed", "Operation Cancelled!", "warning");
                    // handle dismissals
                    // result.dismiss can be 'cancel', 'overlay', 'esc' or 'timer'
                }
            });
            //document.getElementById("wizard1").submit();
        }
    })

})(jQuery);