﻿var regions = [];
var factories = [];

function LoadRegionList(restUrl, selectId) {
    // make an ajax call to refresh the passed in select box
    $.ajax({
        type: "GET",
        url: restUrl,
        data: {
            page: 1,
            size: 1,
            mode: "forSelect"
        },
        success: function (data) {
            //call is successfully completed and we got result in data
            if (data == '') return;
            regions = data;
            LoadRegionSelect(selectId, data);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            //some errror, some show err msg to user and log the error
            alert("Server Error Occurred. Unable to load list of registered regions");
            //Log(xhr.responseText);
        }
    });
}

function LoadFactoriesList(restUrl, regionCode, regionFactorySelectId) {
    // make an ajax call to refresh the passed in select box
    $.ajax({
        type: "GET",
        url: restUrl,
        data: {
            regionCode: regionCode
        },
        success: function (data) {
            //call is successfully completed and we got result in data
            if (data == '') return;
            factories = data;
            LoadFactorySelect(regionFactorySelectId, data);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            //some errror, some show err msg to user and log the error
            alert("Server Error Occurred. Unable to load list of registered factories for region code: " + regionCode);
            //Log(xhr.responseText);
        }
    });
}

function LoadRegionSelect(selectRef, data) {
    $(selectRef).children('option:not(:first)').remove();
    $.each(data, function (index, region) {
        var r = new Option(region.Name, region.Code);
        $(selectRef).append(r);
    });
    //$(selectRef).selectpicker('refresh');
}

function LoadFactorySelect(selectRef, data) {
    $(selectRef).children('option:not(:first)').remove();
    $.each(data, function (index, factory) {
        var f = new Option(factory.Name, factory.Code);
        $(selectRef).append(f);
    });
    //$(selectRef).selectpicker('refresh');
}

function SetPlaceholderText(placeholderId, placeholderText) {
    $(placeholderId).text(placeholderText);
}

function SetRegionFactories(regionCode, regionFactoryDropdownId) {
    $(regionFactoryDropdownId).children('option:not(:first)').remove();
    $.each(factories, function (key, factory) {
        if (factory.RegionCode == regionCode) {
            $(regionFactoryDropdownId).append(
                $("<option/>").val(factory.Code).html(factory.Name));

            //$(regionFactoryDropdownId).selectpicker('refresh');
        }
    });
            // exit the loop
            return false;
    
}
function ResetModuleAndRole(selectModuleRef, selectRoleRef) {
    $(selectModuleRef).children('option:not(:first)').remove();
    $(selectModuleRef).selectpicker('refresh');
    $(selectRoleRef).val('').change();
}