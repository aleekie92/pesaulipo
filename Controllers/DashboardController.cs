﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using pesaulipo.Models;
using pesaulipo.Repository;

namespace pesaulipo.Controllers
{

	public class DashboardController : Controller
	{

		//string maill;
		public ActionResult Home()
		{

			ApplicationUser applicationUser = (new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()))).FindById<ApplicationUser, string>(base.User.Identity.GetUserId());

			if (applicationUser == null)
			{
				return RedirectToAction("Login", "Account");
			}
			else
			{
				DashboardViewModel dashboardViewModel = (new DataHandler()).DashBoardFunctions(applicationUser.UserType, applicationUser.AgentCode, applicationUser.Region, applicationUser.Factory);
				return this.PartialView("Home", dashboardViewModel);
			}




		}

		[Authorize(Roles = "HQ, Administrator, Supervisor, CO")]
		public ActionResult GetAgentsList()
		{
			ApplicationUser applicationUser = (new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()))).FindById<ApplicationUser, string>(base.User.Identity.GetUserId());

			if (applicationUser == null)
			{
				return RedirectToAction("Login", "Account");
			}
			else
			{
				DataHandler dataHandler = new DataHandler();
				AgentViewModel agentViewModel = new AgentViewModel()
				{
					AgentsList = dataHandler.GetSaccoAgentsList(applicationUser.UserType, applicationUser.Region, applicationUser.Factory)
				};
				return base.View(agentViewModel);
			}

		}

		public ActionResult GetUsers(string message = null)
		{
			if (message != null)
			{
				((dynamic)base.ViewBag).Toast = message;
			}
			UsersModel usersModel = new UsersModel();
			DataHandler dataHandler = new DataHandler();
			ApplicationUser applicationUser = (new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()))).FindById<ApplicationUser, string>(base.User.Identity.GetUserId());

			if (applicationUser == null)
			{
				return RedirectToAction("Login", "Account");
			}
			else
			{
				//ViewBag.Message = applicationUser.AgentCode;
				return base.View(dataHandler.ViewAllUsers(applicationUser.AgentCode));
			}

		}

		public ActionResult DeleteUser(string Id)
		{
			if (!(new DataHandler()).DeleteUser(Id))
			{
				return base.Content("Error");
			}
			return base.RedirectToAction("GetUsers", "Dashboard");
		}

		[Authorize(Roles = "Administrator")]
		public ActionResult SwitchUser(string agent_code)
		{
			if (agent_code != null)
			{
				AspNetUser myobject = new AspNetUser();
				myobject.AgentCode = agent_code;
				return base.View(myobject);
			}
			return base.View("Error");

		}

		[HttpPost]
		[Authorize(Roles = "Administrator")]
		public ActionResult SwitchUser(AspNetUser model)
		{
			if (!ModelState.IsValid)
			{
				RedirectToAction("Home");
			}

			using (AgencyBankingDBDataContextDataContext db = new AgencyBankingDBDataContextDataContext())
			{
				AspNetUser userr = db.AspNetUsers.Where(a => a.AgentCode == model.AgentCode).FirstOrDefault();
				if (userr != null)
				{
					//userr.UserType = model.UserType;
					//userr.Region = model.Region;
					//userr.Factory = model.Factory;
					userr.Enabled = model.Enabled;
					db.SubmitChanges();
				}
			}
			return RedirectToAction("GetUsers", "Dashboard");
		}


		[Authorize(Roles = "Administrator")]
		public ActionResult EditUser(string agent_code)
		{
			if (agent_code != null)
			{
				AspNetUser myobject = new AspNetUser();
				myobject.AgentCode = agent_code;
				return base.View(myobject);
			}
			return base.View("Error");

		}
		[HttpPost]
		[Authorize(Roles = "Administrator")]
		public ActionResult EditUser(AspNetUser model)
		{
			if (!ModelState.IsValid)
			{
				RedirectToAction("Home");
			}

			using (AgencyBankingDBDataContextDataContext db = new AgencyBankingDBDataContextDataContext())
			{
				AspNetUser userr = db.AspNetUsers.Where(a => a.AgentCode == model.AgentCode).FirstOrDefault();

				if (userr != null)
				{
					userr.UserType = model.UserType;
					userr.Region = model.Region;
					userr.Factory = model.Factory;

					db.SubmitChanges();
				}
			}

			return base.RedirectToAction("GetUsers", "Dashboard");
		}


		[Authorize(Roles = "Administrator")]
		public ActionResult EditSaccoAgent(string agent_code)
		{
			if (agent_code != null)
			{
				AgentViewModel myobject = new AgentViewModel();
				myobject.AgentCode = agent_code;
				return base.View(myobject);
			}
			return base.View("Error");

		}
		[HttpPost]
		[Authorize(Roles = "Administrator")]
		public ActionResult EditSaccoAgent(AgentViewModel model)
		{
			if (!ModelState.IsValid)
			{
				RedirectToAction("Home");
			}

			using (LiveDataDbContextDataContext db = new LiveDataDbContextDataContext())
			{
				GFL_Live_Agent_Application userr = db.GFL_Live_Agent_Applications.Where(a => a.Agent_Code == model.AgentCode).FirstOrDefault();
				if (userr != null)
				{
					userr.Agent_Code = model.AgentCode;
					userr.Email = model.Email;
					userr.Role = model.UserType;
					userr.RegionCode = model.Region;
					db.SubmitChanges();
				}
			}

			return base.RedirectToAction("GetAgentsList", "Dashboard");
		}

		public FileStreamResult GetPDF()
		{
			ApplicationUser applicationUser = (new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()))).FindById<ApplicationUser, string>(base.User.Identity.GetUserId());
			string agentstatementpath = WebServiceUrl.serviceDataObject.GetAgentStatement("GFL0185");
		    //string agentstatementpathh = "C:\\Users/Alex/Downloads/Documents/membersform.pdf";// WebServiceUrl.serviceDataObject.GetAgentStatement("GFL0185");

			 FileStream fs = new FileStream(agentstatementpath, FileMode.Open, FileAccess.Read);
			 //FileStream fss = new FileStream(agentstatementpathh, FileMode.Open, FileAccess.Read);

			 return File(fs, "application/pdf");
		}

	}
}