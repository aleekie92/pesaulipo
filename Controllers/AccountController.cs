﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using pesaulipo.Models;

namespace pesaulipo.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        AspNetUser userr;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager )
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set 
            { 
                _signInManager = value; 
            }
        }
        
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            
            var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: true);
            ApplicationUser userrr = UserManager.FindByEmail(model.Email);
            switch (result)
            {
                
                case SignInStatus.Success:
                    
                    Session["userid"] = userrr.Id;
                    if (userrr.EmailConfirmed==true) {
                        if (userrr.Enabled == true)
                        {
                            //using (LiveDataDbContextDataContext db = new LiveDataDbContextDataContext())
                            //{
                            //    GFL_Live_Agent_Application agentapplication = db.GFL_Live_Agent_Applications.Where(a => a.Email == model.Email).FirstOrDefault();

                            //    if (agentapplication != null)
                            //    {
                            //      WebServiceUrl.serviceDataObject.SendSms(agentapplication.Mobile_No,"You have successfully logged in to BSS portal");
                            //    }
                            //}
                            return RedirectToLocal(returnUrl, model.Email);
                        }
                        else
                        {
                            ModelState.AddModelError("", "Contact system administrator to Activate your account.");
                            return View(model);
                        }

                    }
                    else
                    {
                        ModelState.AddModelError("", "Login to your staff email to activate your account.");
                        return View(model);
                    }
                  
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    Session["userid"] = userrr.Id;
                    if (userrr.EmailConfirmed == true)
                    {
                        if (userrr.Enabled == true)
                        {
                            DateTime noww = DateTime.Now;
                            using (AgencyBankingDBDataContextDataContext db = new AgencyBankingDBDataContextDataContext())
                            {
                                userr = db.AspNetUsers.Where(a => a.Email == model.Email).FirstOrDefault();

                                if (userr != null)
                                {
                                    userr.Status = true;
                                    userr.LastSuccessLoginTime = noww.ToString();
                                    db.SubmitChanges();
                                }
                            }
                            return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                        }
                        else
                        {
                            ModelState.AddModelError("", "Contact system administrator to Activate your account.");
                            return View(model);
                        }

                    }
                    else
                    {
                        ModelState.AddModelError("", "Login to your staff email to activate your account.");
                        return View(model);
                    }
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
        }

       
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model, LoginViewModel lvm)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

           
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent:  false, rememberBrowser: false);

           
            switch (result)
            {
                case SignInStatus.Success:
                   
                    using (LiveDataDbContextDataContext db = new LiveDataDbContextDataContext())
                    {
                        GFL_Live_Agent_Application agentapplication = db.GFL_Live_Agent_Applications.Where(a => a.Email == lvm.Email).FirstOrDefault();

                        if (agentapplication != null)
                        {
                            WebServiceUrl.serviceDataObject.SendSms(agentapplication.Mobile_No, "Dear " + agentapplication.Name + ", You have successfully logged in to BSS portal.");
                        }
                        return RedirectToLocal(model.ReturnUrl, "");
                    }

                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }
        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterPasswordlessViewModel model)
        {
            ActionResult action;
            ApplicationUser applicationUser;
            if (this.ModelState.IsValid)
            {

                using (LiveDataDbContextDataContext db = new LiveDataDbContextDataContext())
                {
                    GFL_Live_Agent_Application agentapplication = db.GFL_Live_Agent_Applications.Where(a => a.Email == model.Email).FirstOrDefault();

                    if (agentapplication != null)
                    {

                        ApplicationUser applicationUser1 = new ApplicationUser()
                        {
                            UserName = model.Email,
                            Email = model.Email,
                            PhoneNumber = agentapplication.Mobile_No,
                            AgentCode = agentapplication.Agent_Code,
                            Enabled = false,
                            UserType = agentapplication.Role,
                            Region=agentapplication.RegionCode,
                            Factory=agentapplication.Factory_Code,
                            TwoFactorEnabled=true

                        };
                        applicationUser = applicationUser1;
                        try
                        {
                            IdentityResult identityResult = await this.UserManager.CreateAsync(applicationUser);
                            string str = await this.UserManager.GenerateEmailConfirmationTokenAsync(applicationUser.Id);
                            if (this.Request.Url != null)
                            {
                                string str1 = this.Url.Action("ConfirmEmail", "Account", new { userId = applicationUser.Id, code = str }, this.Request.Url.Scheme);
                                await this.UserManager.SendEmailAsync(applicationUser.Id, "Email Confirmation", string.Concat("<\"", str1, "\">"));
                            }
                            if (!identityResult.Succeeded)
                            {
                                this.AddErrors(identityResult);
                                identityResult = null;
                            }
                            else
                            {
                                IdentityResult roleAsync = await this.UserManager.AddToRoleAsync(applicationUser.Id, agentapplication.Role);
                                identityResult = roleAsync;
                                ModelState.AddModelError("", "Account created successfully, contact administrator to activate it.");
                                WebServiceUrl.serviceDataObject.SendSms(agentapplication.Mobile_No, "Your account has been created successfully, contact system admin to activate it.");
                                action = this.RedirectToAction("Login", "Account");

                                return action;
                            }
                        }
                        catch (Exception exception)
                        {
                             action = this.RedirectToAction("Page404", "Utilities");
                            return action;
                        }
                        applicationUser = null;
                    }
                    else
                    {
                        action = this.RedirectToAction("Register", "Account");
                    }
                }
            }
            ModelState.AddModelError("", "Enter the correct staff email address OR contact the system administrator to set it up.");
            //action = this.RedirectToAction("Login", "Account", model);
            //return action;
            return View(model);
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            ActionResult actionResult;
            string str;
            if (userId == null || code == null)
            {
                actionResult = this.View("Error");
            }
            else
            {
                IdentityResult identityResult = await this.UserManager.ConfirmEmailAsync(userId, code);
                ApplicationUser applicationUser = await this.UserManager.FindByIdAsync(userId);
                await this.SignInManager.SignInAsync(applicationUser, true, true);
                AccountController accountController = this;
                str = (identityResult.Succeeded ? "ConfirmEmail" : "Error");
                actionResult = accountController.View(str);
            }
            return actionResult;
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }


        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");
                }

                // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                // string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                // var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);		
                // await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                // return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            //return code == null ? View("Error") : View();
            if (code != null)
            {
                return base.View();
            }
            return base.View("Error");
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            /* if (!ModelState.IsValid)
             {
                 return View(model);
             }
             var user = await UserManager.FindByNameAsync(model.Email);
             if (user == null)
             {
                 // Don't reveal that the user does not exist
                 return RedirectToAction("ResetPasswordConfirmation", "Account");
             }
             var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
             if (result.Succeeded)
             {
                 return RedirectToAction("ResetPasswordConfirmation", "Account");
             }
             AddErrors(result);
             return View();*/

            ActionResult action;
            if (this.ModelState.IsValid)
            {
                ApplicationUser applicationUser = await this.UserManager.FindByNameAsync(model.Email);
                if (applicationUser != null)
                {
                    IdentityResult identityResult = await this.UserManager.ResetPasswordAsync(applicationUser.Id, model.Code, model.Password);
                    IdentityResult identityResult1 = identityResult;
                    if (!identityResult1.Succeeded)
                    {
                        this.AddErrors(identityResult1);
                        action = this.View();
                    }
                    else
                    {
                        action = this.RedirectToAction("ResetPasswordConfirmation", "Account");
                    }
                }
                else
                {
                    action = this.RedirectToAction("ResetPasswordConfirmation", "Account");
                }
            }
            else
            {
                action = this.View(model);
            }
            return action;
        
    }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            
            ActionResult actionResult;
            string verifiedUserIdAsync = await this.SignInManager.GetVerifiedUserIdAsync();
            if (verifiedUserIdAsync != null)
            {
                IList<string> validTwoFactorProvidersAsync = await this.UserManager.GetValidTwoFactorProvidersAsync(verifiedUserIdAsync);
                List<SelectListItem> list = (
                    from purpose in validTwoFactorProvidersAsync
                    select new SelectListItem()
                    {
                        Text = purpose,
                        Value = purpose
                    }).ToList<SelectListItem>();
                AccountController accountController = this;
                SendCodeViewModel sendCodeViewModel = new SendCodeViewModel()
                {
                    Providers = list,
                    ReturnUrl = returnUrl,
                    RememberMe = rememberMe
                };
                actionResult = accountController.View(sendCodeViewModel);
            }
            else
            {
                actionResult = this.View("Error");
            }
            return actionResult;
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
           

            ActionResult action;
            if (!this.ModelState.IsValid)
            {
                action = this.View();
            }
            else if (await this.SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                //this.SignInManager.UserManager.GetValidTwoFactorProviders();
                action = this.RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
            }
            else
            {
                action = this.View("Error");
            }
            return action;
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl, "");
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl, "");
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        [HttpGet]
        public ActionResult Signout()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            using (AgencyBankingDBDataContextDataContext db = new AgencyBankingDBDataContextDataContext())
            {
               ApplicationUser signedInUser = UserManager.FindById(Session["userid"].ToString());
                userr = db.AspNetUsers.Where(a => a.Email == signedInUser.Email).FirstOrDefault();
               
                if (userr != null)
                {
                    userr.Status = false;
                    db.SubmitChanges();
                }
            }
            Session.Clear();
            Session.Abandon();
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Login", "Account");
        }

        

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl, string email)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            DateTime noww = DateTime.Now;
            using (AgencyBankingDBDataContextDataContext db = new AgencyBankingDBDataContextDataContext())
            {
                userr = db.AspNetUsers.Where(a => a.Email == email).FirstOrDefault();
                if (userr != null)
                {
                    userr.Status = true;
                    userr.LastSuccessLoginTime = noww.ToString();
                   
                    db.SubmitChanges();
                }
            }

           return RedirectToAction("Home", "Dashboard");

        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}