﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using pesaulipo.Models;
using pesaulipo.Repository;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json;
using System.Linq.Expressions;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Data.Linq;

namespace pesaulipo.Controllers
{
    public class TransactionsController : Controller
    {
        public TransactionsController()
        {
        }

        [Authorize(Roles = "HQ, Administrator, Supervisor, CO, Agent")]
        public ActionResult index(WebServiceTransactionModel agencyTransactionsModel, int id = 984659846)
        {
            DateTime dateTime;
            AgencyTransactionsModels list = new AgencyTransactionsModels();
          
            WebServiceTransactionModel list2 = new WebServiceTransactionModel();
            
            UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            ApplicationUser applicationUser = userManager.FindById<ApplicationUser, string>(base.User.Identity.GetUserId());

            AgencyBankingDBDataContextDataContext _context1 = new AgencyBankingDBDataContextDataContext();
            LiveDataDbContextDataContext _context2 = new LiveDataDbContextDataContext();

            DateTime? nullablee = null;
            DateTime? nullable1 = null;
            string transactionTypeFilter = "";
            string agentFilter = null;

            string regionFilter = null;
            string factoryFilter = null;
            string statusfilter = null;

            foreach (TransactionType transs in (
                 from cp in _context1.TransactionTypes
                 select cp).ToList<TransactionType>())
            {
                list2.transactionType.Add(new SelectListItem()
                {
                    Text = transs.Name,
                    Value = transs.Name.ToString()
                });
            }

            foreach (string str in new List<string>()
            {
                "Open",
                "Pending",
                "Approved",
                "Rejected"
            })
            {
                list2.StatusDropDown.Add(new SelectListItem()
                {
                    Text = str,
                    Value = str
                });
            }

            if (User.IsInRole("HQ") || User.IsInRole("Administrator")) {

                foreach (Region regionss in (
              from cp in _context1.Regions
              select cp).ToList<Region>())
                {
                    list2.RegionDropDownForm.Add(new SelectListItem()
                    {
                        Text = regionss.Name,
                        Value = regionss.Code.ToString()
                    }); ;
                }

                foreach (GFL_Live_Agent_Application agent in (
                   from cp in _context2.GFL_Live_Agent_Applications
                   select cp).ToList<GFL_Live_Agent_Application>())
                {
                    list2.AgentDropDownForm.Add(new SelectListItem()
                    {
                        Text = agent.Name,
                        Value = agent.Agent_Code
                    });
                }

                foreach (Factory factos in (
               from cp in _context1.Factories
               select cp).ToList<Factory>())
                {
                    list2.FactoryDropDownForm.Add(new SelectListItem()
                    {
                        Text = factos.Name,
                        Value = factos.Code.ToString()
                    }); ;
                }
            }
            if (User.IsInRole("Supervisor"))
            {
                foreach (GFL_Live_Agent_Application agent in (
               from cp in _context2.GFL_Live_Agent_Applications
               where cp.RegionCode == applicationUser.Region
               select cp).ToList<GFL_Live_Agent_Application>())
                {
                    list2.AgentDropDownForm.Clear();
                    list2.AgentDropDownForm.Add(new SelectListItem()
                    {
                        Text = agent.Name,
                        Value = agent.Agent_Code
                    });
                }

                foreach (Factory factos in (
               from cp in _context1.Factories
               where cp.RegionCode == applicationUser.Region
               select cp).ToList<Factory>())
                {
                    list2.FactoryDropDownForm.Add(new SelectListItem()
                    {
                        Text = factos.Name,
                        Value = factos.Code.ToString()
                    }); ;
                }
            }
            if (User.IsInRole("CO"))
            {
                foreach (GFL_Live_Agent_Application agent in (
               from cp in _context2.GFL_Live_Agent_Applications
               where cp.Factory_Code == applicationUser.Factory
               select cp).ToList<GFL_Live_Agent_Application>())
                {
                    list2.AgentDropDownForm.Clear();
                    list2.AgentDropDownForm.Add(new SelectListItem()
                    {
                        Text = agent.Name,
                        Value = agent.Agent_Code
                    });
                }

               
            }

           
            if (agencyTransactionsModel.DateFilterFrom != null)
            {
                string str2 = Regex.Replace(agencyTransactionsModel.DateFilterFrom, "\\s+", "");
                try
                {
                    nullablee = new DateTime?(Convert.ToDateTime(str2));
                }
                catch (Exception exception)
                {
                    nullablee = null;
                }
            }
            if (agencyTransactionsModel.DateFilterTo != null)
            {
                string str3 = Regex.Replace(agencyTransactionsModel.DateFilterTo, "\\s+", "");
                try
                {
                    dateTime = Convert.ToDateTime(str3);
                    nullable1 = new DateTime?(dateTime.AddDays(1));
                }
                catch (Exception exception1)
                {
                    nullable1 = null;
                }
            }
            if (id != 984659846)
            {
                if (id != 1)
                {
                    int num = id * -1;
                    dateTime = DateTime.Now;
                    nullablee = new DateTime?(dateTime.AddDays((double)num));
                }
                else
                {
                    nullablee = new DateTime?(DateTime.Today);
                }
            }
            if (agencyTransactionsModel.TransactionTypeFilter != "")
            {
                transactionTypeFilter = agencyTransactionsModel.TransactionTypeFilter;
            }
            if (agencyTransactionsModel.AgentFilter != null)
            {
                agentFilter = agencyTransactionsModel.AgentFilter;
            }

            if (agencyTransactionsModel.RegionFilter != null)
            {
                regionFilter = agencyTransactionsModel.RegionFilter;

                foreach (Factory factos in (
              from cp in _context1.Factories
              where cp.RegionCode==regionFilter
              select cp).ToList<Factory>())
                {
                    list2.FactoryDropDownForm.Add(new SelectListItem()
                    {
                        Text = factos.Name,
                        Value = factos.Code.ToString()
                    }); ;
                }
            }

            if (agencyTransactionsModel.FactoryFilter != null)
            {
                factoryFilter = agencyTransactionsModel.FactoryFilter;
            }

            if(agencyTransactionsModel.StatusFilter != null)
            {
                statusfilter = agencyTransactionsModel.StatusFilter;
            }

            DataHandler dataHandler = new DataHandler();
            list2.transactionsList = (
                from cc in dataHandler.GetAllAgencyTransactions(nullablee, nullable1, transactionTypeFilter, agentFilter, regionFilter, factoryFilter, applicationUser.UserType, applicationUser.AgentCode, applicationUser.Region,applicationUser.Factory, statusfilter)
                orderby cc.transaction_date descending
                select cc).ToList<WebServiceTransactionModel>();

            list2.AgentListNew = _context2.GFL_Live_Agent_Applications.ToList<GFL_Live_Agent_Application>();

            ViewBag.Message = id;
            return base.View(list2);
        }

    }
}