﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using pesaulipo.Models;
using pesaulipo.Repository;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace pesaulipo.Controllers
{
    public class DeviceController : Controller
    {
        // GET: Device

        [Authorize(Roles = "HQ, Administrator, Supervisor, CO, Agent")]
        public ActionResult Index()
        {
            AgencyBankingDBDataContextDataContext db = new AgencyBankingDBDataContextDataContext();
            List<DeviceModel> deviceModels = new List<DeviceModel>();

            ApplicationUser applicationUser = (new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()))).FindById<ApplicationUser, string>(base.User.Identity.GetUserId());

            if (applicationUser.UserType.Equals("HQ") || applicationUser.UserType.Equals("Administrator"))
            {
                foreach (Device list in (
                       from x in db.Devices
                       select x).ToList<Device>())
                {
                    DeviceModel deviceModel = new DeviceModel()
                    {
                        Name = list.Name,
                        SerialNo = list.SerialNo,
                        TypeCode = list.TypeCode,
                        Enabled = true,
                        Assigned = true,
                        AssignedTo = list.AssigedTo

                    };
                    deviceModels.Add(deviceModel);
                }
            }

            if (applicationUser.UserType.Equals("Supervisor"))
            {
                foreach (Device list in (
                   from x in db.Devices
                   where x.Region == applicationUser.Region
                   select x).ToList<Device>())
                {
                    DeviceModel deviceModel = new DeviceModel()
                    {
                        Name = list.Name,
                        SerialNo = list.SerialNo,
                        TypeCode = list.TypeCode,
                        Enabled = true,
                        Assigned = true,
                        AssignedTo = list.AssigedTo

                    };
                    deviceModels.Add(deviceModel);
                }
            }
            if (applicationUser.UserType.Equals("CO"))
            {
                foreach (Device list in (
                     from x in db.Devices
                     where x.Factory==applicationUser.Factory
                     select x).ToList<Device>())
                {
                    DeviceModel deviceModel = new DeviceModel()
                    {
                        Name = list.Name,
                        SerialNo = list.SerialNo,
                        TypeCode = list.TypeCode,
                        Enabled = true,
                        Assigned = true,
                        AssignedTo = list.AssigedTo

                    };
                    deviceModels.Add(deviceModel);
                }
            }

            if (applicationUser.UserType.Equals("Agent"))
            {
                foreach (Device list in (
                     from x in db.Devices
                     where x.AssigedTo == applicationUser.AgentCode
                     select x).ToList<Device>())
                {
                    DeviceModel deviceModel = new DeviceModel()
                    {
                        Name = list.Name,
                        SerialNo = list.SerialNo,
                        TypeCode = list.TypeCode,
                        Enabled = true,
                        Assigned = true,
                        AssignedTo = list.AssigedTo

                    };
                    deviceModels.Add(deviceModel);
                }
            }


            return View(deviceModels);
        }


       

        // GET: /Account/Register
        [HttpGet]
        public ActionResult RegisterDevice()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public ActionResult RegisterDevice(Device devicemodel)
        {

            UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            ApplicationUser applicationUser = userManager.FindById<ApplicationUser, string>(base.User.Identity.GetUserId());
            AgencyBankingDBDataContextDataContext db2 = new AgencyBankingDBDataContextDataContext();
            using (LiveDataDbContextDataContext db = new LiveDataDbContextDataContext())
            {

                GFL_Live_Agent_Application deviceInfor = db.GFL_Live_Agent_Applications.Where(a => a.Agent_Code == devicemodel.AssigedTo).FirstOrDefault();

                Device device = new Device
                {
                    Name = devicemodel.Name,
                    SerialNo = devicemodel.SerialNo,
                    Enabled = true,
                    TypeCode = devicemodel.TypeCode,
                    Assiged = true,
                    AssigedTo = devicemodel.AssigedTo,
                    Region = deviceInfor.RegionCode,
                    Factory = deviceInfor.Factory_Code
                };
                db2.Devices.InsertOnSubmit(device); db.SubmitChanges();
               // db.Devices.InsertOnSubmit(device);
                db.SubmitChanges();
            }

            return base.RedirectToAction("Home", "Dashboard");
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult UpdateDevice(string deviceID)
        {
            if (deviceID != null)
            {
                return base.View();
            }
            return base.View("Error");

        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public ActionResult UpdateDevice(Device model)
        {
            if (!ModelState.IsValid)
            {
                RedirectToAction("Home");
            }

            using (AgencyBankingDBDataContextDataContext db = new AgencyBankingDBDataContextDataContext())
            {
                Device deviceInfor = db.Devices.Where(a => a.SerialNo == model.SerialNo).FirstOrDefault();

                if (deviceInfor != null)
                {
                    deviceInfor.AssigedTo = model.AssigedTo;
                    deviceInfor.SerialNo = model.SerialNo;

                    db.SubmitChanges();
                }
            }

            ViewBag.Message = String.Format("Device "+model.SerialNo+ " Updated succesifully");
            return base.RedirectToAction("Index", "Device");
        }
    }
}