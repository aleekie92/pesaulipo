﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using pesaulipo.Repository;


namespace pesaulipo.Controllers
{
    public class RegionFactoryController : Controller
    {

        [HttpGet]
        [Authorize]
        public ActionResult GetRegionFactory(string regionCode)
        {
            DataHandler dl = new DataHandler();

            if (string.IsNullOrEmpty(regionCode))
            {
                return Json(dl.GetFactories().ToArray(), JsonRequestBehavior.AllowGet);
            }
            else
            {
                IEnumerable<Factory> regionFactories = dl.GetFactories()
                  .Where(f => f.RegionCode.Equals(regionCode))
                  .OrderBy(c => c.Name)
                  .ToArray();
                return Json(regionFactories, JsonRequestBehavior.AllowGet);
            }
        }
    }
}