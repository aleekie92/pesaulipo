﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using pesaulipo.Models;
using pesaulipo.Repository;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Text.RegularExpressions;

namespace pesaulipo.Controllers
{
    public class ReportsController : Controller
    {
		[Authorize(Roles = "HQ, Administrator, Supervisor, CO, Agent")]
		public ActionResult ViewReport(Reports reports, int id = 94865132)
		{
			DateTime dateTime;
			//Reports report = new Reports();
			DateTime? nullable = null;
			DateTime? nullable1 = null;
			UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
			ApplicationUser applicationUser = userManager.FindById<ApplicationUser, string>(base.User.Identity.GetUserId());
			
			AgencyBankingDBDataContextDataContext agencyBankingDBDataContext = new AgencyBankingDBDataContextDataContext();
			Reports report1 = new Reports();
			ApplicationUser applicationUserr = (new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()))).FindById<ApplicationUser, string>(base.User.Identity.GetUserId());

			DataHandler dataHandler = new DataHandler();
			
			
			
			if (id != 94865132)
			{
				int num = id * -1;
				dateTime = DateTime.Now;
				nullable = new DateTime?(dateTime.AddDays((double)num));
			}
			
			string str3 = "";
			//string str4 = "Anonymous";
			if (base.User.IsInRole("Supervisor"))
			{
				str3 = "Supervisor";
			}
		
			else if (base.User.IsInRole("HQ"))
			{
				str3 = "HQ";
				
			}
			else if (base.User.IsInRole("Administrator"))
			{
				str3 = "Administrator";
				
			}
			else if (base.User.IsInRole("Agent"))
			{
				str3 = "Agent";
		
			}
			else if (base.User.IsInRole("CO"))
			{
				str3 = "CO";

			}
			report1.ReportStorage = dataHandler.Reports(str3, nullable, nullable1, null, null, applicationUserr.AgentCode, applicationUserr.Region, applicationUserr.Factory);

			report1.AllSaccosBalanceEnquiryCoretecShare = 0;
			Reports allSaccosBalanceEnquiryCount = report1;
			allSaccosBalanceEnquiryCount.AllSaccosBalanceEnquiryCoretecShare = allSaccosBalanceEnquiryCount.ReportStorage.AllSaccosBalanceEnquiryCount * 5;
			report1.AllSaccosLoanRepaymentCoretecShare = 0;
			report1.AllSaccosMemberRegistrationCoretecShare = 0;
			Reports allSaccosMinistatementCount = report1;
			allSaccosMinistatementCount.AllSaccosMinistatementCoretecShare = allSaccosMinistatementCount.ReportStorage.AllSaccosMinistatementCount * 5;
			report1.AllSaccosTransactionsCoretecShare = 0;
			Reports particularSaccoWithdrawalCount1001000 = report1;
			particularSaccoWithdrawalCount1001000.AllSaccosMemberactivationCoretecShare = particularSaccoWithdrawalCount1001000.ReportStorage.ParticularSaccoMemberactivationCount1001000 * -25 + report1.ReportStorage.ParticularSaccoMemberactivationCount10013000 * -25 + report1.ReportStorage.ParticularSaccoMemberactivationCount30016000 * -25 + report1.ReportStorage.ParticularSaccoMemberactivationCount600110000 * -25 + report1.ReportStorage.ParticularSaccoMemberactivationCount1000015000 * -30 + report1.ReportStorage.ParticularSaccoMemberactivationCount1500125000 * -30;
			Reports particularSaccoDepositCount501000 = report1;
			particularSaccoDepositCount501000.AllSaccosLoanApplicationCoretecShare = particularSaccoDepositCount501000.ReportStorage.ParticularSaccoLoanApplicationCount501000 * 10 + report1.ReportStorage.ParticularSaccoLoanApplicationCount10003000 * 10 + report1.ReportStorage.ParticularSaccoLoanApplicationCount30006000 * 15 + report1.ReportStorage.ParticularSaccoLoanApplicationCount600010000 * 15 + report1.ReportStorage.ParticularSaccoLoanApplicationCount1000015000 * 15 + report1.ReportStorage.ParticularSaccoLoanApplicationCount2500050000 * 20 + report1.ReportStorage.ParticularSaccoLoanApplicationCount50000100000 * 30;
			return base.View(report1);
		}
	}
}