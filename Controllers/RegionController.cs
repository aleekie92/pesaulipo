﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using pesaulipo.Models;
using pesaulipo.Repository;

namespace pesaulipo.Controllers
{
    public class RegionController : Controller
    {
        [HttpGet]
        [Authorize]
        public ActionResult GetRegisteredRegions(string mode = "default")
        {
            DataHandler dl = new DataHandler();

                    IEnumerable<Region> registeredRegionsRecordsForSelect = dl.GetRegions()
                      .OrderBy(c => c.Code)
                      .ToArray();
                    return Json(registeredRegionsRecordsForSelect, JsonRequestBehavior.AllowGet);
          
        }
    }
}